<?php

namespace App\Controller;

use App\Entity\Incident;
use App\Entity\Reservation;
use App\Entity\Spot;
use App\Entity\Task;
use App\Form\SpotType;
use App\Repository\HistoryRepository;
use App\Repository\SpotRepository;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/spot")
 * @IsGranted("ROLE_USER")
 */
class SpotController extends AbstractController
{
    private $historyRepository;
    private $pattern = "Emplacement n°%s";
    private $em;
    private $paginator;
    private $spotRepository;

    public function __construct(HistoryRepository $historyRepository, EntityManagerInterface $em,
                                PaginatorInterface $paginator, SpotRepository $spotRepository)
    {
        $this->historyRepository = $historyRepository;
        $this->em = $em;
        $this->paginator = $paginator;
        $this->spotRepository = $spotRepository;
    }

    /**
     * @Route("/listUsed", name="spot_list_used")
     */
    public function listUsed(Request $request): Response
    {
        $spots = $this->paginator->paginate($this->spotRepository->findSpotUsed(),
            $request->query->getInt('page', 1), 10);

        $this->historyRepository->log(HistoryRepository::READ_ALL, sprintf($this->pattern, "ALL_USED"), $this->getUser());

        return $this->render('spot/index.html.twig', [
            'spots' => $spots,
            'button' => true,
            'locations' => $this->getLocations($spots),
        ]);
    }

    /**
     * @Route("/listAvailable", name="spot_list_available")
     */
    public function listAvailable(Request $request): Response
    {
        $spots = $this->paginator->paginate($this->spotRepository->findSpotAvailable(),
            $request->query->getInt('page', 1), 10);

        $this->historyRepository->log(HistoryRepository::READ_ALL, sprintf($this->pattern, "ALL_AVAIL"), $this->getUser());

        return $this->render('spot/index.html.twig', [
            'spots' => $spots,
            'button' => true,
            'locations' => $this->getLocations($spots),
        ]);
    }


    /**
     * @Route("/new", name="spot_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $spot = new Spot();
        $form = $this->createForm(SpotType::class, $spot, ['hidelog' => true]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($spot);
            $this->addFlash(
                'success',
                'Nouvel emplacement créé!'
            );
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::CREATE, sprintf($this->pattern, $spot->getId()), $this->getUser());
            return $this->redirectToRoute('spot_new');
        }

        if(!$form->isSubmitted())
        {
            $form->get('isReserved')->setData(true);
            $form->get('isAvailable')->setData(true);
        }

        return $this->render('spot/new.html.twig', [
            'spot' => $spot,
            'locations' => $this->getLocations(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * @return array Array with longitude and latitude of all spots
     */
    private function getLocations($spots = null): array
    {
        if ($spots == null) {
            $spots = $this->getDoctrine()->getRepository(Spot::class)->findAll();
        }
        $locations = [];

        for ($i = 0; $i < sizeof($spots); $i++) {
            $tabTmp = [];
            $tabTmp[] = $spots[$i]->getLongitude();
            $tabTmp[] = $spots[$i]->getLatitude();
            $tabTmp[] = $spots[$i]->getId();
            $locations[] = $tabTmp;
        }

        return $locations;
    }

    /**
     * @Route("/{id}", name="spot_show", methods={"GET"})
     */
    public function show(Spot $spot, TaskRepository $taskRepository): Response
    {

        $locations = $this->getLocations();

        $currentLocation = [
            [
                $spot->getLongitude(),
                $spot->getLatitude(),
                $spot->getId()
            ]
        ];

        $tasks = $taskRepository->findBySpot($spot)->getResult();

        $this->historyRepository->log(HistoryRepository::READ, sprintf($this->pattern, $spot->getId()), $this->getUser());

        return $this->render('spot/show.html.twig', [
            'spot' => $spot, 'tasks' => $tasks,
            'locations' => $locations,
            'currentLocation' => $currentLocation,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="spot_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Spot $spot): Response
    {
        $form = $this->createForm(SpotType::class, $spot, array(
            'hidelog' => false
        ));

        $form->remove('isReserved'); 
        $form->remove('isAvailable'); 

        $form->handleRequest($request);
        $locations = $this->getLocations();
        $currentLocation = array(array($spot->getLongitude(), $spot->getLatitude(), $spot->getId()));


        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::UPDATE, sprintf($this->pattern, $spot->getId()), $this->getUser());
            return $this->redirectToRoute('spotPage');
        }

        return $this->render('spot/edit.html.twig', [
            'spot' => $spot,
            'locations' => $locations,
            'currentLocation' => $currentLocation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="spot_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Spot $spot): Response
    {
        if ($this->isCsrfTokenValid('delete' . $spot->getId(), $request->request->get('_token'))) {

            $incident = $this->getDoctrine()->getRepository(Incident::class)->findAll();

            foreach ($incident as $i){
                if($i->getSpot() === $spot){

                    $tasks = $this->getDoctrine()->getRepository(Task::class)->findAll();

                    foreach ($tasks as $t){
                        if($t->getIncident() === $i){
                            $this->em->remove($t);
                        }
                    }

                    $this->em->remove($i);
                }
            }

            $reservation = $this->getDoctrine()->getRepository(Reservation::class)->findAll();

            foreach ($reservation as $i){
                if($i->getSpot() === $spot){
                    $this->em->remove($i);
                }
            }

            $this->em->remove($spot);
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::DELETE, sprintf($this->pattern, $spot->getId()), $this->getUser());
        }

        return $this->redirectToRoute('spotPage');
    }
}
