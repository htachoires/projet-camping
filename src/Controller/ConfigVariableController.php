<?php

namespace App\Controller;

use App\Entity\ConfigVariable;
use App\Form\ConfigVariableType;
use App\Repository\ConfigVariableRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/configuration")
 * @IsGranted("ROLE_ADMIN")
 */
class ConfigVariableController extends AbstractController
{
    /**
     * @Route("/", name="config_variable_index", methods={"GET"})
     */
    public function index(ConfigVariableRepository $configVariableRepository): Response
    {
        return $this->render('config_variable/index.html.twig', [
            'config_variables' => $configVariableRepository->findAll(),
        ]);
    }

    /**
     * @Route("/reset", name="config_variable_reset", methods={"GET"})
     */
    public function reset()
    {
        $names = ["ARRIVAL_HOUR_START_MIN", "ARRIVAL_HOUR_START_MAX",
            "END_HOUR_START_MIN", "END_HOUR_START_MAX",
            "START_WORK_HOUR", "END_WORK_HOUR"];

        $values = [$_ENV["ARRIVAL_HOUR_START_MIN"] ?? 8, $_ENV["ARRIVAL_HOUR_START_MAX"] ?? 11,
            $_ENV["END_HOUR_START_MIN"] ?? 8, $_ENV["END_HOUR_START_MAX"] ?? 11,
            $_ENV["START_WORK_HOUR"] ?? 6, $_ENV["END_WORK_HOUR"] ?? 21
        ];
        $em = $this->getDoctrine()->getManager();

        $variables = $em->getRepository(ConfigVariable::class)->findAll();

        foreach ($variables as $variable) {
            $em->remove($variable);
        }

        for ($i = 0; $i < count($names); $i++) {
            $variable = new ConfigVariable();
            $variable->setName($names[$i])
                ->setValue($values[$i]);
            $em->persist($variable);
        }
        $em->flush();

        return $this->redirectToRoute("config_variable_index");
    }

    /**
     * @Route("/new", name="config_variable_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $configVariable = new ConfigVariable();
        $form = $this->createForm(ConfigVariableType::class, $configVariable);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($configVariable);
            $entityManager->flush();

            return $this->redirectToRoute('config_variable_index');
        }

        return $this->render('config_variable/new.html.twig', [
            'config_variable' => $configVariable,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="config_variable_show", methods={"GET"})
     */
    public function show(ConfigVariable $configVariable): Response
    {
        return $this->render('config_variable/show.html.twig', [
            'config_variable' => $configVariable,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="config_variable_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, ConfigVariable $configVariable): Response
    {
        $name = $configVariable->getName();
        $form = $this->createForm(ConfigVariableType::class, $configVariable);
        $form->handleRequest($request);
        $configVariable->setName($name);
        if ($form->isSubmitted() && $form->isValid()) {
            $configVariable->setUpdatedAt(new \DateTime());
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('config_variable_index');
        }

        return $this->render('config_variable/edit.html.twig', [
            'config_variable' => $configVariable,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="config_variable_delete", methods={"DELETE"})
     */
    public function delete(Request $request, ConfigVariable $configVariable): Response
    {
        if ($this->isCsrfTokenValid('delete' . $configVariable->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($configVariable);
            $entityManager->flush();
        }

        return $this->redirectToRoute('config_variable_index');
    }
}
