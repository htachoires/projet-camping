<?php

namespace App\Controller;

use App\Entity\Spot;
use App\Form\SpotType;
use App\Repository\HistoryRepository;
use App\Repository\ReservationRepository;
use App\Repository\TaskRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\PieChart;
use CMEN\GoogleChartsBundle\GoogleCharts\Charts\AreaChart;
use App\Entity\Product;
use App\Entity\Reservation;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * @Route("/stat")
 * @IsGranted("ROLE_USER")
 */
class StatController extends AbstractController
{
    private $reservationRepository;
    private $em;

    public function __construct(ReservationRepository $reservationRepository, EntityManagerInterface $em)
    {
        $this->reservationRepository = $reservationRepository;
        $this->em = $em;
    }

    /**
     * @Route("/", name="stat_index", methods={"GET"})
     */
    public function index(): Response
    {
        $productChart = new PieChart();

        $products = $this->getDoctrine()->getRepository(Product::class)->findAll();

        $array = array(
            ['Produit', 'Nombre de vente'],
        );

        foreach ($products as $produit){
            array_push($array, [$produit->getName(), $produit->getCommands()->count()]);
        }

        $productChart->getData()->setArrayToDataTable( $array );
        $productChart->getOptions()->setIs3D(true);
        $productChart->getOptions()->setTitle('Produit les plus vendus');
        $productChart->getOptions()->setHeight(400);
        //$productChart->getOptions()->setWidth(500);
        $productChart->getOptions()->getTitleTextStyle()->setFontSize(25);

        $area = new AreaChart();


        $reservations = $this->getDoctrine()->getRepository(Reservation::class)->findAll();

        $yearsArray = [];

        foreach ($reservations as $r){

            $year = intval($r->getStartDate()->format('Y'));

            if(!in_array($year, $yearsArray))
                array_push($yearsArray, $year);
        }

        
        $array2 = array(
            ['Années', 'Nombre de reservation'],
        );

        foreach ($yearsArray as $year){

            $date = new \DateTime();
            $date->setDate($year, 0, 0);

            $reservationByYear = $this->reservationRepository
                ->findByDate($date)
                ->getResult();

            array_push($array2, [$year, count($reservationByYear)]);
        }

        $area->getData()->setArrayToDataTable($array2);
        
        $area->getOptions()->setTitle('Nombre de reservation par année');
        $area->getOptions()->getHAxis()->setTitle('Year');
        $area->getOptions()->getHAxis()->getTitleTextStyle()->setColor('#333');
        $area->getOptions()->setHeight(400);
        //$area->getOptions()->setWidth(500);
        $area->getOptions()->getTitleTextStyle()->setFontSize(25);

        return $this->render('stat/index.html.twig', 
            [
                'produit' => $productChart,
                'reservation' => $area
            ]
        );
    }
}