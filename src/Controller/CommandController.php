<?php

namespace App\Controller;

use App\Entity\Command;
use App\Form\CommandType;
use App\Repository\HistoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/command")
 * @IsGranted("ROLE_USER")
 */
class CommandController extends AbstractController
{
    private $historyRepository;
    private $pattern = "Commande n°%s";
    private $em;

    public function __construct(HistoryRepository $historyRepository, EntityManagerInterface $em)
    {
        $this->historyRepository = $historyRepository;
        $this->em = $em;
    }

    /**
     * @Route("/", name="command_index", methods={"GET"})
     */
    public function index(): Response
    {
        $commands = $this->getDoctrine()
            ->getRepository(Command::class)
            ->findAll();
        $this->historyRepository->log(HistoryRepository::READ_ALL, sprintf($this->pattern, "ALL"), $this->getUser());

        return $this->render('command/index.html.twig', [
            'commands' => $commands,
        ]);
    }

    /**
     * @Route("/new", name="command_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $command = new Command();
        $form = $this->createForm(CommandType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($command);
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::CREATE, sprintf($this->pattern, $command->getId()), $this->getUser());
            return $this->redirectToRoute('command_index');
        }

        return $this->render('command/new.html.twig', [
            'command' => $command,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="command_show", methods={"GET"})
     */
    public function show(Command $command): Response
    {
        $this->historyRepository->log(HistoryRepository::READ, sprintf($this->pattern, $command->getId()), $this->getUser());
        return $this->render('command/show.html.twig', [
            'command' => $command,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="command_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Command $command): Response
    {
        $form = $this->createForm(CommandType::class, $command);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::UPDATE, sprintf($this->pattern, $command->getId()), $this->getUser());
            return $this->redirectToRoute('command_index');
        }

        return $this->render('command/edit.html.twig', [
            'command' => $command,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="command_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Command $command): Response
    {
        if ($this->isCsrfTokenValid('delete' . $command->getId(), $request->request->get('_token'))) {
            $this->em->remove($command);
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::DELETE, sprintf($this->pattern, $command->getId()), $this->getUser());
        }

        return $this->redirectToRoute('command_index');
    }

}
