<?php

namespace App\Controller;

use App\Entity\Reservation;
use App\Form\ReservationType;
use App\Repository\HistoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use App\Form\SearchReservationnType;
use Dompdf\Dompdf;
use Dompdf\Options;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/reservation")
 * @IsGranted("ROLE_USER")
 */
class ReservationController extends AbstractController
{
    private $historyRepository;
    private $pattern = "Réservation n°%s";
    private $em;
    private $paginator;

    public function __construct(HistoryRepository $historyRepository, PaginatorInterface $paginator, EntityManagerInterface $em)
    {
        $this->historyRepository = $historyRepository;
        $this->em = $em;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/list", name="reservation_index")
     */
    public function index(Request $request): Response
    {
        $reservations = $this->getDoctrine()
            ->getRepository(Reservation::class)
            ->findAll();

        $reservationsPaged = $this->paginator->paginate($reservations, $request->query->getInt('page', 1), 10);

        $this->historyRepository->log(HistoryRepository::READ_ALL, sprintf($this->pattern, "ALL"), $this->getUser());

        return $this->render('reservation/index.html.twig', [
            'reservations' => $reservationsPaged,
        ]);
    }

    /**
     * @Route("/listNow", name="reservation_index_now")
     */
    public function listNow(Request $request, EntityManagerInterface $em): Response
    {
        $queryBuilder = $em->getRepository(Reservation::class)->createQueryBuilder('r')
            ->where('r.startDate > CURRENT_DATE() OR (r.endDate > CURRENT_DATE() AND r.startDate < CURRENT_DATE())');
        $reservations = $queryBuilder->getQuery()->getResult();

        $this->historyRepository->log(HistoryRepository::READ_ALL, sprintf($this->pattern, "ALL"), $this->getUser());

        $reservationsPaged = $this->paginator->paginate($reservations, $request->query->getInt('page', 1), 10);

        return $this->render('reservation/index.html.twig', [
            'reservations' => $reservationsPaged,
        ]);
    }

    /**
     * @Route("/new", name="reservation_new")
     */
    public function new(Request $request): Response
    {
        $reservation = new Reservation();
        $form = $this->createForm(ReservationType::class, $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($reservation);
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::CREATE, sprintf($this->pattern, $reservation->getId()), $this->getUser());
            return $this->redirectToRoute('reservation_index');
        }

        return $this->render('reservation/new.html.twig', [
            'reservation' => $reservation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="reservation_show")
     */
    public function show(Reservation $reservation): Response
    {
        return $this->render('reservation/show.html.twig', [
            'reservation' => $reservation,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="reservation_edit")
     */
    public function edit(Request $request, Reservation $reservation): Response
    {
        $form = $this->createForm(ReservationType::class, $reservation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::UPDATE, sprintf($this->pattern, $reservation->getId()), $this->getUser());
            return $this->redirectToRoute('reservation_index');
        }

        return $this->render('reservation/edit.html.twig', [
            'reservation' => $reservation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/search", name="reservation_search")
     */
    public function search(Request $request): Response
    {
        $form = $this->createForm(SearchReservationnType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $name = $form->get("search")->getData();

            return $this->redirectToRoute('reservation_searchList', [
                'request' => $request,
                'name' => $name,
            ]);
        }

        return $this->render('reservation/search.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/searchList", name="reservation_searchList")
     */
    public function searchList(Request $request): Response
    {
        $name = $request->get('name');

        if (!isset($name)) {
            return $this->redirectToRoute('reservation_search');
        }

        $this->historyRepository->log(HistoryRepository::SEARCH, sprintf("Recherche réservation '%s'", $name), $this->getUser());

        $clients = $this->paginator->paginate(
            $this->personRepository->findByName($name . "%"),
            $request->query->getInt('page', 1),
            10
        );

        return $this->render('reservation/index.html.twig', [
            'persons' => $clients,
        ]);
    }

    /**
     * @Route("/{id}", name="reservation_delete")
     */
    public function delete(Request $request, Reservation $reservation): Response
    {
        if ($this->isCsrfTokenValid('delete' . $reservation->getId(), $request->request->get('_token'))) {
            
            $this->em->remove($reservation);
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::DELETE, sprintf($this->pattern, $reservation->getId()), $this->getUser());
        }

        return $this->redirectToRoute('reservation_index');
    }

    /**
     * @Route("/{id}/pdf", name="reservation_pdf")
     */
    public function pdf(Reservation $reservation): Response
    {
        // Configure Dompdf according to your needs
        $pdfOptions = new Options();
        $pdfOptions->set('defaultFont', 'Courier');

        // Instantiate Dompdf with our options
        $dompdf = new Dompdf($pdfOptions);

        // Retrieve the HTML generated in our twig file
        $html = $this->renderView('reservation/showToPdf.html.twig', [
            'reservation' => $reservation,
        ]);
        // Load HTML to Dompdf
        $dompdf->loadHtml($html);

        // (Optional) Setup the paper size and orientation 'portrait' or 'portrait'
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();

        $response = new Response();

        $response->setContent($dompdf->output());
        $response->setStatusCode(200);
        $response->headers->set('Content-Type', 'application/pdf');

        return $response;
    }
}
