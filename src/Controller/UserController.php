<?php

namespace App\Controller;

use App\Entity\History;
use App\Entity\User;
use App\Form\EditUserType;
use App\Form\UserType;
use App\Repository\HistoryRepository;
use App\Repository\PersonRepository;
use App\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/interns")
 * @IsGranted("ROLE_ADMIN")
 */
class UserController extends AbstractController
{
    private $historyRepository;
    private $pattern = "Produit n°%s";
    private $em;
    private $encoder;
    private $paginator;

    public function __construct(HistoryRepository $historyRepository, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder, PaginatorInterface $paginator)
    {
        $this->historyRepository = $historyRepository;
        $this->em = $em;
        $this->encoder = $passwordEncoder;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/list", name="user_index", methods={"GET"})
     */
    public function index(UserRepository $userRepository): Response
    {
        $this->historyRepository->log(HistoryRepository::READ_ALL, sprintf($this->pattern, "ALL"), $this->getUser());
        return $this->render('user/index.html.twig', [
            'users' => $userRepository->findAll(),
        ]);
    }

    /**
     * @Route("/internsList", name="user_interns_index", methods={"GET"})
     */
    public function internsList(PersonRepository $personRepository, Request $request): Response
    {
        $interns = $this->paginator->paginate($personRepository->getInterns(), $request->query->getInt('page', 1), 10);

        $this->historyRepository->log(HistoryRepository::READ_ALL, sprintf($this->pattern, "ALL_INTERNS"), $this->getUser());

        return $this->render('person/index.html.twig', [
            'persons' => $interns,
            'client' => false,
            'reservation' => NULL,
        ]);
    }

    /**
     * @Route("/register", name="user_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $user = new User();
        $form = $this->createForm(UserType::class, $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setPassword($this->encoder->encodePassword($user, $user->getPassword()));
            $this->em->persist($user);
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::CREATE, sprintf($this->pattern, $user->getId()), $this->getUser());
            return $this->redirectToRoute('user_interns_index');
        }

        return $this->render('user/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     */
    public function show(User $user): Response
    {
        $this->historyRepository->log(HistoryRepository::READ, sprintf($this->pattern, $user->getId()), $this->getUser());
        return $this->render('user/show.html.twig', [
            'user' => $user,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, User $user): Response
    {
        $form = $this->createForm(EditUserType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $newPassword = $form->get("newPassword")->getData();
            $user->setPassword($this->encoder->encodePassword($user, $newPassword));
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::UPDATE, sprintf($this->pattern, $user->getId()), $this->getUser());
            return $this->redirectToRoute('user_index');
        }

        return $this->render('user/edit.html.twig', ['user' => $user,
            'form' => $form->createView(),]);
    }

    /**
     * @Route("/{id}", name="user_delete", methods={"DELETE"})
     */
    public function delete(Request $request, User $user): Response
    {
        if ($this->isCsrfTokenValid('delete' . $user->getId(), $request->request->get('_token'))) {

            $history = $this->getDoctrine()->getRepository(History::class)->findAll();

            foreach ($history as $h){
                if($h->getUser() === $user){
                    $this->em->remove($h);
                }
            }

            $this->em->remove($user);
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::DELETE, sprintf($this->pattern, $user->getId()), $this->getUser());
        }

        return $this->redirectToRoute('user_index');
    }

}
