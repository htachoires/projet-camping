<?php

namespace App\Controller;

use App\Entity\Incident;
use App\Entity\Task;
use App\Form\IncidentType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\HistoryRepository;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/incident")
 * @IsGranted("ROLE_USER")
 */
class IncidentController extends AbstractController
{
    private $historyRepository;
    private $pattern = "Incidant n°%s";
    private $paginator;

    public function __construct(HistoryRepository $historyRepository, PaginatorInterface $paginator)
    {
        $this->historyRepository = $historyRepository;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/", name="incident_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $incidents = $this->getDoctrine()
            ->getRepository(Incident::class)
            ->findAll();

        $incidentsPaged = $this->paginator->paginate($incidents, $request->query->getInt('page', 1), 30);

        $this->historyRepository->log(HistoryRepository::READ_ALL, sprintf($this->pattern, "ALL"), $this->getUser());

        return $this->render('incident/index.html.twig', [
            'incidents' => $incidentsPaged,
        ]);
    }

    /**
     * @Route("/new", name="incident_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $incident = new Incident();
        $form = $this->createForm(IncidentType::class, $incident);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($incident);
            $entityManager->flush();

            $this->historyRepository->log(HistoryRepository::CREATE, sprintf($this->pattern, $incident->getId()), $this->getUser());

            return $this->redirectToRoute('incident_index');
        }

        return $this->render('incident/new.html.twig', [
            'incident' => $incident,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="incident_show", methods={"GET"})
     */
    public function show(Incident $incident): Response
    {
        $this->historyRepository->log(HistoryRepository::READ, sprintf($this->pattern, $incident->getId()), $this->getUser());

        return $this->render('incident/show.html.twig', [
            'incident' => $incident,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="incident_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Incident $incident): Response
    {
        $form = $this->createForm(IncidentType::class, $incident);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->historyRepository->log(HistoryRepository::UPDATE, sprintf($this->pattern, $incident->getId()), $this->getUser());

            return $this->redirectToRoute('incident_index');
        }

        return $this->render('incident/edit.html.twig', [
            'incident' => $incident,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="incident_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Incident $incident): Response
    {
        if ($this->isCsrfTokenValid('delete'.$incident->getId(), $request->request->get('_token'))) {

            $entityManager = $this->getDoctrine()->getManager();

            $tasks = $this->getDoctrine()->getRepository(Task::class)->findAll();

            foreach ($tasks as $t){
                if($t->getIncident() === $incident){
                    $entityManager->remove($t);
                }
            }


            $entityManager->remove($incident);
            $entityManager->flush();
            $this->historyRepository->log(HistoryRepository::DELETE, sprintf($this->pattern, $incident->getId()), $this->getUser());
        }

        return $this->redirectToRoute('incident_index');
    }
}
