<?php

namespace App\Controller;

use App\Entity\Incident;
use App\Entity\Task;
use App\Form\TaskType;
use App\Repository\HistoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/task")
 * @IsGranted("ROLE_USER")
 */
class TaskController extends AbstractController
{
    private $historyRepository;
    private $pattern = "Incident n°%s";
    private $em;
    private $paginator;

    public function __construct(HistoryRepository $historyRepository, EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->historyRepository = $historyRepository;
        $this->em = $em;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/", name="task_index", methods={"GET"})
     */
    public function index(Request $request): Response
    {
        $tasks = $this->em->getRepository(Task::class)->findBy([], ['state' => 'ASC']);

        $tasksPaged = $this->paginator->paginate($tasks, $request->query->getInt('page', 1), 20);

        $this->historyRepository->log(HistoryRepository::READ_ALL, sprintf($this->pattern, "ALL"), $this->getUser());

        return $this->render('task/index.html.twig', [
            'tasks' => $tasksPaged,
        ]);
    }

    /**
     * @Route("/new", name="task_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $task = new Task();
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $this->em->persist($task->getIncident());
            $this->em->persist($task);
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::CREATE, sprintf($this->pattern, $task->getId()), $this->getUser());
            return $this->redirectToRoute('task_index');
        }

        return $this->render('task/new.html.twig', [
            'task' => $task,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="task_show", methods={"GET"})
     */
    public function show(Task $task): Response
    {
        $this->historyRepository->log(HistoryRepository::READ, sprintf($this->pattern, $task->getId()), $this->getUser());
        return $this->render('task/show.html.twig', [
            'task' => $task,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="task_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Task $task): Response
    {
        $form = $this->createForm(TaskType::class, $task);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::UPDATE, sprintf($this->pattern, $task->getId()), $this->getUser());
            return $this->redirectToRoute('task_index');
        }

        return $this->render('task/edit.html.twig', [
            'task' => $task,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="task_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Task $task): Response
    {
        if ($this->isCsrfTokenValid('delete' . $task->getId(), $request->request->get('_token'))) {
            $this->em->remove($task);
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::DELETE, sprintf($this->pattern, $task->getId()), $this->getUser());
        }

        return $this->redirectToRoute('task_index');
    }
}
