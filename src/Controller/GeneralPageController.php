<?php

namespace App\Controller;

use App\Entity\Button;
use App\Entity\History;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @IsGranted("ROLE_USER")
 */
class GeneralPageController extends AbstractController
{
    /**
     * @Route("/", name="homePage")
     * 
     */
    public function index()
    {
        $buttons = [];

        $this->addButtonToArray($buttons, "Clients", "people", $this->generateUrl('clientHomePage'));

        $this->addButtonToArray($buttons, "Reservations", "class", $this->generateUrl('reservationHomePage'));

        $this->addButtonToArray($buttons, "Emplacements", "location_on", $this->generateUrl('spotPage'));

        $this->addButtonToArray($buttons, "Statistiques", "data_usage", $this->generateUrl('stat_index'));

        $this->addButtonToArray($buttons, "Stocks", "shopping_cart", $this->generateUrl('product_index'));

        if ($this->getUser()->getIsAdmin()) {
            $this->addButtonToArray($buttons, "Stagiaires", "person", $this->generateUrl('internsHomePage'));
        }
        else
        {
            $this->addButtonToArray($buttons, "Mes heures", "schedule", $this->generateUrl('work_period_person_show', [ 'id' => $this->getUser()->getId() ]));
        }

        return $this->render('/index.html.twig',
            ['buttons' => $buttons, 'button' => true]);
    }

    /**
     * @Route("/lost", name="lost")
     */
    public function lost()
    {
        // If logged, show 404 page
        if ($this->getUser()) {
            return $this->render('lost.html.twig');
        }

        // otherwise redirect to login
        return $this->redirectToRoute('app_login', ['last_username' => NULL, 'error' => NULL]);
    }

    public function addButtonToArray(&$array, $name, $icon, $link)
    {
        $button = new Button();
        $button->setName($name);
        $button->setIcon($icon);
        $button->setLink($link);
        array_push($array, $button);
    }

    /**
     * @Route("/reservation", name="reservationHomePage")
     */
    public function reservationHomePage()
    {
        $buttons = [];

        $this->addButtonToArray($buttons, "Nouvelle réservation", "assignment", $this->generateUrl('reservation_new'));

        $this->addButtonToArray($buttons, "Liste des réservation en cours", "list", $this->generateUrl('reservation_index_now'));

        $this->addButtonToArray($buttons, "Liste de toutes les réservation", "list", $this->generateUrl('reservation_index'));

        $this->addButtonToArray($buttons, "Liste de toutes les réservation par client", "list", $this->generateUrl('person_index'));

        return $this->render('/index.html.twig',
            ['buttons' => $buttons, 'button' => true]);
    }

    /**
     * @Route("/client", name="clientHomePage")
     */
    public function clientHomePage()
    {
        $buttons = [];

        $this->addButtonToArray($buttons, "Ajouter un client", "assignment", $this->generateUrl('person_new_client'));

        $this->addButtonToArray($buttons, "Retrouver un client", "search", $this->generateUrl('person_search'));

        $this->addButtonToArray($buttons, "Liste des clients", "list", $this->generateUrl('person_index'));

        return $this->render('/index.html.twig',
            ['buttons' => $buttons, 'button' => true]);
    }

    /**
     * @Route("/spot", name="spotPage")
     */
    public function spotPage()
    {
        $buttons = [];

        $this->addButtonToArray($buttons, "Ajouter un emplacement", "location_on", $this->generateUrl('spot_new'));

        $this->addButtonToArray($buttons, "Liste des emplacements occupés", "list", $this->generateUrl('spot_list_used'));

        $this->addButtonToArray($buttons, "Liste des emplacements libres", "list", $this->generateUrl('spot_list_available'));

        $this->addButtonToArray($buttons, "Ajouter une tache", "add_circle_outline", $this->generateUrl('task_new'));

        $this->addButtonToArray($buttons, "Liste des taches", "list", $this->generateUrl('task_index'));

        $this->addButtonToArray($buttons, "Ajouter un incident", "add_circle_outline", $this->generateUrl('incident_new'));

        $this->addButtonToArray($buttons, "Liste des incidents", "list", $this->generateUrl('incident_index'));

        return $this->render('/index.html.twig',
            ['buttons' => $buttons, 'button' => true]);
    }

    /**
     * @Route("/interns", name="internsHomePage")
     * @IsGranted("ROLE_ADMIN")
     */
    public function internsHomePage()
    {
        $buttons = [];

        $this->addButtonToArray($buttons, "Ajouter un profil stagiaire", "add_circle_outline", $this->generateUrl('person_new_interns'));

        $this->addButtonToArray($buttons, "Créer une session stagiaire", "add_circle_outline", $this->generateUrl('user_new'));

        $this->addButtonToArray($buttons, "Liste des sessions", "assignment", $this->generateUrl('user_index'));

        //$this->addButtonToArray($buttons, "Modifier un stagiaire", "assignment", "#");

        $this->addButtonToArray($buttons, "Liste des stagiaires", "assignment", $this->generateUrl('user_interns_index'));

        $this->addButtonToArray($buttons, "Heure de travail par stagiaire", "schedule", $this->generateUrl('work_period_users_index')); //

        $this->addButtonToArray($buttons, "Liste des heures de travail", "schedule", $this->generateUrl('work_period_index')); //

        $this->addButtonToArray($buttons, "Historique", "history", $this->generateUrl('history'));

        return $this->render('/index.html.twig',
            ['buttons' => $buttons, 'button' => true]);
    }


    public function commitLog(string $message)
    {
        $entityManager = $this->getDoctrine()->getManager();

        $log = new History();
        $log->setDate(new \DateTime('now'));
        $log->setDescription($message);
        $log->setUser($this->getUser());

        $entityManager->persist($log);

        $entityManager->flush();
    }
}
