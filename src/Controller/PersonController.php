<?php

namespace App\Controller;


use App\Entity\Person;
use App\Entity\Reservation;
use App\Form\InternType;
use App\Form\PersonType;
use App\Form\SearchPersonType;
use App\Repository\HistoryRepository;
use App\Repository\PersonRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/client")
 * @IsGranted("ROLE_USER")
 */
class PersonController extends AbstractController
{
    private $historyRepository;
    private $pattern = "Personne n°%s";
    private $em;
    private $paginator;
    private $personRepository;

    public function __construct(HistoryRepository $historyRepository, EntityManagerInterface $em,
                                PaginatorInterface $paginator, PersonRepository $personRepository)
    {
        $this->historyRepository = $historyRepository;
        $this->em = $em;
        $this->personRepository = $personRepository;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/list", name="person_index", methods={"GET"})
     */
    public function index(Request $request, PersonRepository $personRepository): Response
    {
        $clients = $this->paginator->paginate($personRepository->getClients(), $request->query->getInt('page', 1), 10);

        $this->historyRepository->log(HistoryRepository::READ_ALL, sprintf($this->pattern, "ALL"), $this->getUser());

        return $this->render('person/index.html.twig', [
            'persons' => $clients,
            'client' => true,
        ]);
    }

    /**
     * @Route("/new", name="person_new_client")
     */
    public function new(Request $request): Response
    {
        $person = new Person();
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $this->isCsrfTokenValid('person-token', $request->request->get('person')['_token'])) {
            $this->em->persist($person);
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::CREATE, sprintf($this->pattern, $person->getId()), $this->getUser());
            return $this->redirectToRoute('person_index');
        }

        return $this->render('person/new.html.twig', [
            'person' => $person,
            'error' => NULL,
            'client' => true,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/internsNew", name="person_new_interns")
     * @IsGranted("ROLE_ADMIN")
     */
    public function internsNew(Request $request): Response
    {
        $person = new Person();
        $form = $this->createForm(InternType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->em->persist($person);
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::CREATE, sprintf($this->pattern, $person->getId()), $this->getUser());

            return $this->redirectToRoute('internsHomePage');
        }

        return $this->render('person/new.html.twig', [
            'person' => $person,
            'error' => NULL,
            'client' => false,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/search", name="person_search")
     */
    public function search(Request $request): Response
    {
        $form = $this->createForm(SearchPersonType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $name = $form->get("search")->getData();

            return $this->redirectToRoute('person_searchList', [
                'request' => $request,
                'name' => $name,
            ]);
        }

        return $this->render('person/search.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/searchList", name="person_searchList")
     */
    public function searchList(Request $request): Response
    {
        $name = $request->get('name');
        if (!isset($name)) {
            return $this->redirectToRoute('person_search');
        }

        $this->historyRepository->log(HistoryRepository::SEARCH, sprintf("Recherche client '%s'", $name), $this->getUser());

        $clients = $this->paginator->paginate($this->personRepository->findByName($name . "%"),
            $request->query->getInt('page', 1), 10);

        return $this->render('person/index.html.twig', [
            'persons' => $clients,
            'client' => true,
        ]);
    }

    /**
     * @Route("/{id}", name="person_show")
     */
    public function show(Person $person): Response
    {
        $reservations = $this->getDoctrine()->getRepository(Reservation::class)->findBy(['person' => $person]);
        $this->historyRepository->log(HistoryRepository::READ, sprintf($this->pattern, $person->getId()), $this->getUser());

        return $this->render('person/show.html.twig', [
            'person' => $person,
            'reservation' => $reservations,
            'client' => true
        ]);
    }

    /**
     * @Route("/inters/{id}", name="person_show_interns")
     * @IsGranted("ROLE_ADMIN")
     */
    public function showIntern(Person $person): Response
    {
        $this->historyRepository->log(HistoryRepository::READ, sprintf($this->pattern, $person->getId()), $this->getUser());

        return $this->render('person/show.html.twig', [
            'person' => $person,
            'reservation' => NULL,
            'client' => false
        ]);
    }

    /**
     * @Route("/{id}/edit", name="person_edit")
     */
    public function edit(Request $request, Person $person): Response
    {
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $this->isCsrfTokenValid('person-token', $request->request->get('person')['_token'])) {
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::UPDATE, sprintf($this->pattern, $person->getId()), $this->getUser());
            return $this->redirectToRoute('person_index');
        }

        return $this->render('person/edit.html.twig', [
            'person' => $person,
            'client' => true,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}/editInterns", name="person_edit_interns")
     */
    public function editInterns(Request $request, Person $person): Response
    {
        $form = $this->createForm(PersonType::class, $person);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid() && $this->isCsrfTokenValid('person-token', $request->request->get('person')['_token'])) {
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::UPDATE, sprintf($this->pattern, $person->getId()), $this->getUser());
            return $this->redirectToRoute('person_index');
        }

        return $this->render('person/edit.html.twig', [
            'person' => $person,
            'client' => false,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="person_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Person $person): Response
    {
        if ($this->isCsrfTokenValid('delete' . $person->getId(), $request->request->get('_token'))) {
            $this->em->remove($person);
            $this->em->flush();
            $this->historyRepository->log(HistoryRepository::DELETE, sprintf($this->pattern, $person->getId()), $this->getUser());
        }

        return $this->redirectToRoute('person_index');
    }
}
