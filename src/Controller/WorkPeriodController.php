<?php

namespace App\Controller;

use App\Entity\Person;
use App\Entity\WorkPeriod;
use App\Form\WorkPeriodType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\HistoryRepository;
use App\Repository\PersonRepository;
use App\Repository\WorkPeriodRepository;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;

/**
 * @Route("/work_period")
 */
class WorkPeriodController extends AbstractController
{
    private $historyRepository;
    private $em;
    private $encoder;
    private $paginator;
    private $pattern = "Horaire n°%s";

    public function __construct(HistoryRepository $historyRepository, EntityManagerInterface $em, PaginatorInterface $paginator)
    {
        $this->historyRepository = $historyRepository;
        $this->em = $em;
        $this->paginator = $paginator;
    }

    /**
     * @Route("/", name="work_period_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function index(Request $request): Response
    {
        $workPeriods = 
            $this->paginator->paginate($this->getDoctrine()
            ->getRepository(WorkPeriod::class)
            ->findAll(), $request->query->getInt('page', 1), 10);

        return $this->render('work_period/index.html.twig', [
            'work_periods' => $workPeriods,
        ]);
    }

    /**
     * @Route("/users", name="work_period_users_index", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function indexUsers(PersonRepository $personRepository, Request $request): Response
    {
        $interns = $this->paginator->paginate($personRepository->getInterns(), $request->query->getInt('page', 1), 10);

        $this->historyRepository->log(HistoryRepository::READ_ALL, sprintf($this->pattern, "ALL_INTERNS_WORK_PERIODS"), $this->getUser());

        return $this->render('person/index.html.twig', [
            'persons' => $interns,
            'client' => false,
            'reservation' => NULL,
            'workPeriod' => true,
        ]);
    }

    /**
     * @Route("/new", name="work_period_new", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function new(Request $request): Response
    {
        $workPeriod = new WorkPeriod();
        $form = $this->createForm(WorkPeriodType::class, $workPeriod);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($workPeriod);
            $entityManager->flush();

            return $this->redirectToRoute('work_period_index');
        }

        return $this->render('work_period/new.html.twig', [
            'work_period' => $workPeriod,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="work_period_person_show", methods={"GET"})
     * @IsGranted("ROLE_USER")
     */
    public function showForPerson(WorkPeriodRepository $workRepository, Person $person, Request $request): Response
    {
        # if the intern is not watching it's own periods
        if ($this->getUser() && !$this->getUser()->getIsAdmin() && $this->getUser()->getId() != $person->getId()) {
            return $this->render('lost.html.twig');
        }

        $workPeriods = $this->paginator->paginate($workRepository->getWorkPeriod($person), $request->query->getInt('page', 1), 10);

        $this->historyRepository->log(HistoryRepository::READ_ALL, sprintf($this->pattern, $person->getId()), $this->getUser());

        return $this->render('work_period/index.html.twig', [
            'work_periods' => $workPeriods,
            'person' => $person,
        ]);
    }


    /**
     * @Route("/work_period/{id}", name="work_period_show", methods={"GET"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function show(WorkPeriod $workPeriod): Response
    {
        return $this->render('work_period/show.html.twig', [
            'work_period' => $workPeriod,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="work_period_edit", methods={"GET","POST"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function edit(Request $request, WorkPeriod $workPeriod): Response
    {
        $form = $this->createForm(WorkPeriodType::class, $workPeriod);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('work_period_index');
        }

        return $this->render('work_period/edit.html.twig', [
            'work_period' => $workPeriod,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="work_period_delete", methods={"DELETE"})
     * @IsGranted("ROLE_ADMIN")
     */
    public function delete(Request $request, WorkPeriod $workPeriod): Response
    {
        if ($this->isCsrfTokenValid('delete'.$workPeriod->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($workPeriod);
            $entityManager->flush();
        }

        return $this->redirectToRoute('work_period_index');
    }
}
