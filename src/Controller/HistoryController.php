<?php

namespace App\Controller;

use App\Entity\History;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/history")
 * @IsGranted("ROLE_ADMIN")
 */
class HistoryController extends AbstractController
{
    private $paginator;

    public function __construct(PaginatorInterface $paginator)
    {
        $this->paginator = $paginator;
    }

    /**
     * @Route("/", name="history")
     */
    public function index(Request $request, EntityManagerInterface $em)
    {
        $history = $em->getRepository(History::class)->findBy([], ['date' => 'DESC']);

        $historyPaged = $this->paginator->paginate($history, $request->query->getInt('page', 1), 30);

        return $this->render('history/index.html.twig', [
            'histories' => $historyPaged,
        ]);
    }
}
