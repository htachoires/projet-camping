<?php


namespace App\Repository;


use App\Entity\History;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\ORMException;

/**
 * @method History|null find($id, $lockMode = null, $lockVersion = null)
 * @method History|null findOneBy(array $criteria, array $orderBy = null)
 * @method History[]    findAll()
 * @method History[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class HistoryRepository extends ServiceEntityRepository
{
    const CREATE = 'CRÉATION';
    const READ_ALL = 'AFFICHAGE_COMPLET';
    const READ = 'AFFICHAGE';
    const UPDATE = 'ÉDITION';
    const DELETE = 'SUPPRESSION';
    const SEARCH = 'RECHERCHE';

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, History::class);
    }

    public function log(string $type, string $message, User $user): bool
    {
        $history = new History();
        $pattern = "[%s] %s";
        $history->setDescription(sprintf($pattern, $type, $message));
        $history->setUser($user);

        try {
            $this->getEntityManager()->persist($history);
            $this->getEntityManager()->flush($history);
            return true;
        } catch (ORMException $e) {
            return false;
        }
    }

}