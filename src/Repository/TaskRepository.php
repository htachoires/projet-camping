<?php


namespace App\Repository;


use App\Entity\Incident;
use App\Entity\Spot;
use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }

    public function findBySpot(Spot $spot): Query
    {
        return $this->createQueryBuilder('t')
            ->innerJoin(Incident::class, 'i', 'WITH', 'i.id = t.incident')
            ->where('i.spot = ' . $spot->getId())->OrderBy('t.state', 'ASC')->getQuery();
    }

}