<?php


namespace App\Repository;


use App\Entity\Person;
use App\Entity\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\ORM\Query;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Person|null find($id, $lockMode = null, $lockVersion = null)
 * @method Person|null findOneBy(array $criteria, array $orderBy = null)
 * @method Person[]    findAll()
 * @method Person[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
final class PersonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        // The second parameter is the Entity this Repository uses
        parent::__construct($registry, Person::class);
    }

    /**
     * @return Query Query of all interns in the database
     */
    public function getInterns(): Query
    {
        return $this->createQueryBuilder('p')
            ->innerJoin(User::class, 'user', 'WITH', 'p.id = user.person')
            ->where('user.isAdmin = 0')->getQuery();
    }

    /**
     * @return Query Query of all clients in the database
     */
    public function getClients(): Query
    {
        return $this->createQueryBuilder('p')
            ->leftJoin(User::class, 'user', 'WITH', 'p.id = user.person')
            ->where('user.person IS NULL')->getQuery();
    }

    public function findByName(string $name): Query
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.firstName  like :name OR p.lastName  like :name')
            ->setParameter('name', $name . '%')
            ->getQuery();
    }

    /**
     * Add a simple save method so you don't need to use persist and flush in your service classes
     * @param Person $person
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function save(Person $person)
    {
        // _em is EntityManager which is DI by the base class
        $this->_em->persist($person);
        $this->_em->flush();
    }
}