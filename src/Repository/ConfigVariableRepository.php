<?php

namespace App\Repository;

use App\Entity\ConfigVariable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method ConfigVariable|null find($id, $lockMode = null, $lockVersion = null)
 * @method ConfigVariable|null findOneBy(array $criteria, array $orderBy = null)
 * @method ConfigVariable[]    findAll()
 * @method ConfigVariable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConfigVariableRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ConfigVariable::class);
    }

    // /**
    //  * @return ConfigVariable[] Returns an array of ConfigVariable objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('c.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?ConfigVariable
    {
        return $this->createQueryBuilder('c')
            ->andWhere('c.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
