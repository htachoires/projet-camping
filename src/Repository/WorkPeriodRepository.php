<?php


namespace App\Repository;

use App\Entity\Person;
use App\Entity\WorkPeriod;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method WorkPeriod|null find($id, $lockMode = null, $lockVersion = null)
 * @method WorkPeriod|null findOneBy(array $criteria, array $orderBy = null)
 * @method WorkPeriod[]    findAll()
 * @method WorkPeriod[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class WorkPeriodRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, WorkPeriod::class);
    }

    public function getWorkPeriod(Person $p)
    {
        return $this->createQueryBuilder('w')
            ->innerJoin(Person::class, 'p', 'WITH', 'w.person = p.id')
            ->where('p.id = ' . $p->getId());
    }

    public function getWorkPeriods()
    {
        return $this->getDoctrine()
        ->getRepository(WorkPeriod::class)
        ->findAll();
    }

}