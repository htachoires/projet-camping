<?php


namespace App\Repository;


use App\Entity\Reservation;
use App\Entity\Spot;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\Query;

/**
 * @method Spot|null find($id, $lockMode = null, $lockVersion = null)
 * @method Spot|null findOneBy(array $criteria, array $orderBy = null)
 * @method Spot[]    findAll()
 * @method Spot[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SpotRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Spot::class);
    }

    public function findSpotUsed(): Query
    {
        return $this->createQueryBuilder('s')
            ->innerJoin(Reservation::class, 'r', 'WITH', 's.id = r.spot')
            ->where('r.startDate > CURRENT_DATE() OR (r.endDate > CURRENT_DATE() AND r.startDate < CURRENT_DATE())')->getQuery();
    }

    public function findSpotAvailable(): Query
    {
        $qb = $this->_em->createQueryBuilder();
        $sub = $this->createQueryBuilder('s')
        ->innerJoin(Reservation::class, 'r', 'WITH', 's.id = r.spot')
        ->where('r.startDate > CURRENT_DATE() OR (r.endDate > CURRENT_DATE() AND r.startDate < CURRENT_DATE())')->getQuery();

        $final = $this->createQueryBuilder('h')
            ->where($qb->expr()->notIn('h.id',  $sub->getDQL()))
            ->getQuery();

        return $final;
    }
}
