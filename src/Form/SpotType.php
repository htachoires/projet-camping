<?php

namespace App\Form;

use App\Entity\Spot;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SpotType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $hideLog = NULL;

        if ($options['hidelog']) $hideLog = HiddenType::class;

        $builder
            ->add('comment', NULL, array('label' => 'Commentaire'))
            ->add('size', NULL, array('label' => 'Taille'))
            ->add('maxCapacity', NULL, array('label' => 'Capacité maximale'))
            ->add('isReserved', HiddenType::class, array('label' => 'Est réservé', 'required' => false))
            ->add('isAvailable', HiddenType::class, array('label' => 'Est disponible', 'required' => false))
            ->add('hasBathroom', NULL, array('label' => 'Salle de bain', 'required' => false))
            ->add('hasToilet', NULL, array('label' => 'Toilettes', 'required' => false))
            ->add('hasShade', NULL, array('label' => 'Ombragé', 'required' => false))
            ->add('handicapFriendly', NULL, array('label' => 'Accesibilité handicap', 'required' => false))
            ->add('hasElectricity', NULL, array('label' => 'Electricité', 'required' => false))
            ->add('longitude', $hideLog, array('label' => 'Longitude'))
            ->add('latitude', $hideLog, array('label' => 'Latitude'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Spot::class,
        ]);

        $resolver->setRequired(array(
            'hidelog'
        ));
    }
}
