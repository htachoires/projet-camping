<?php

namespace App\Form;

use App\Entity\Product;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ProductType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', NULL, array('label' => 'Nom'))
            ->add('quantity', NULL, array('label' => 'Quantité'))
            ->add('price', NULL, array('label' => 'Prix'))
            ->add('soldTo', NULL, array('label' => 'Vendu à'))
            ->add('category', NULL, array('label' => 'Categorie'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
        ]);
    }
}
