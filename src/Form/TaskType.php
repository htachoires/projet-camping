<?php

namespace App\Form;

use App\Entity\Task;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('description', NULL, array('label' => 'Description'))
            ->add('state', NULL, array('label' => 'Etat'))
            ->add('startDate', NULL, array('label' => 'Date de début'))
            ->add('endDate', NULL, array('label' => 'Date de fin'))
            ->add('incident', NULL, array('label' => 'Incident'))
            ->add('user', NULL, array('label' => 'Utilisateur'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);
    }
}
