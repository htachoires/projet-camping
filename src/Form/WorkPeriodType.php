<?php

namespace App\Form;

use App\Entity\WorkPeriod;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class WorkPeriodType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('date', NULL, array('label' => 'Date'))
            ->add('startHour', NULL, array('label' => 'Heure de départ'))
            ->add('pauseHour', NULL, array('label' => 'Heure de pause'))
            ->add('resumeHour', NULL, array('label' => 'Heure de reprise'))
            ->add('endHour', NULL, array('label' => 'Heure de fin'))
            ->add('person', NULL, array('label' => 'Stagiaire'))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => WorkPeriod::class,
        ]);
    }
}
