<?php

namespace App\Form;

use App\Entity\Reservation;
use App\Entity\Spot;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ReservationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('startDate', NULL, array('label' => 'Date de début'))
            ->add('endDate', NULL, array('label' => 'Date de fin'))
            ->add('price', NULL, array('label' => 'Prix'))
            ->add('personCount', NULL, array('label' => 'Nombre de personne'))
            ->add('comment', NULL, array('label' => 'Commentaire'))
            ->add('person', NULL, array('label' => 'Identifiant client'))
            ->add('spot', NULL, array('label' => 'Spot réservé', 'query_builder' => function (EntityRepository $er) {
                $qb = $er->createQueryBuilder('m');
                $sub = $er->createQueryBuilder('s')
                    ->innerJoin(Reservation::class, 'r', 'WITH', 's.id = r.spot')
                    ->where('r.startDate > CURRENT_DATE() OR (r.endDate > CURRENT_DATE() AND r.startDate < CURRENT_DATE())')->getQuery();

                $final = $er->createQueryBuilder('h')
                    ->where($qb->expr()->notIn('h.id',  $sub->getDQL()));

                return $final;
            },));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Reservation::class,
        ]);
    }
}
