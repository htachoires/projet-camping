<?php

namespace App\Form;

use App\Entity\Person;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InternType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('firstName', NULL, array('label' => 'Nom'))
            ->add('lastName', NULL, array('label' => 'Prénom'))
            ->add('address', NULL, array('label' => 'Adresse'))
            ->add('email', NULL, array('label' => 'Email'))
            ->add('phone', TelType::class, array('label' => 'Téléphone'))
            ->add('iban', NULL, array('label' => 'IBAN'))
            ->add('secu_num', NULL, array('label' => 'Numero sécurité sociale'))
            ->add('birthDate', BirthdayType::class, [
                'placeholder' => [
                    'day' => 'Jour', 'month' => 'Mois', 'year' => 'Année'
                ], 'label' => 'Date de naissance'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Person::class,
            'csrf_protection' => true,
            // the name of the hidden HTML field that stores the token
            'csrf_field_name' => '_token',
            // an arbitrary string used to generate the value of the token
            // using a different string for each form improves its security
            'csrf_token_id' => 'person-token',
        ]);
    }
}
