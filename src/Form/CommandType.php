<?php

namespace App\Form;

use App\Entity\Command;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommandType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('bill', NULL, array('label' => 'Facture'))
            ->add('commandDate', NULL, array('label' => 'Date de commande'))
            ->add('deliveryDate', NULL, array('label' => 'Date de livraison'))
            ->add('product', NULL, array('label' => 'Produit'));
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Command::class,
        ]);
    }
}
