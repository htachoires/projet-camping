<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Incident
 *
 * @ORM\Table(name="incident", indexes={@ORM\Index(name="spotI_id", columns={"spot_id"})})
 * @ORM\Entity
 */
class Incident
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=255, nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $comment;

    /**
     * @var int
     *
     * @ORM\Column(name="important", type="integer", nullable=false)
     * @Assert\Range(min="0")
     * @Assert\NotNull()
     */
    private $important;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     * @Assert\NotNull()
     */
    private $date;

    /**
     * @var Spot
     *
     * @ORM\ManyToOne(targetEntity="Spot")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="spot_id", referencedColumnName="id")
     * })
     * @Assert\NotNull()
     */
    private $spot;

    public function __construct()
    {
        $this->setDate(new \DateTime());
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getImportant(): ?int
    {
        return $this->important;
    }

    public function setImportant(?int $important): self
    {
        $this->important = $important;

        return $this;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(?\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getSpot(): ?Spot
    {
        return $this->spot;
    }

    public function setSpot(?Spot $spot): self
    {
        $this->spot = $spot;

        return $this;
    }

    public function __toString(): string
    {
        return $this->id;
    }


}
