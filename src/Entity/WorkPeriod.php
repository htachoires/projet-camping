<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * WorkPeriod
 *
 * @ORM\Table(name="work_period", indexes={@ORM\Index(name="personWP_id", columns={"person_id"})})
 * @ORM\Entity
 */
class WorkPeriod
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="date", type="datetime", nullable=false)
     * @Assert\NotNull()
     */
    private $date;

    /**
     * @var int
     *
     * @ORM\Column(name="start_hour", type="integer", nullable=false)
     * @Assert\NotNull()
     */
    private $startHour;

    /**
     * @var int
     *
     * @ORM\Column(name="pause_hour", type="integer", nullable=false)
     * @Assert\NotNull()
     */
    private $pauseHour;

    /**
     * @var int
     *
     * @ORM\Column(name="resume_hour", type="integer", nullable=false)
     * @Assert\NotNull()
     */
    private $resumeHour;

    /**
     * @var int
     *
     * @ORM\Column(name="end_hour", type="integer", nullable=false)
     * @Assert\NotNull()
     */
    private $endHour;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     * })
     * @Assert\NotNull()
     */
    private $person;

    public function __construct()
    {
        $this->setDate(new \DateTime());
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getDate(): ?\DateTimeInterface
    {
        return $this->date;
    }

    public function setDate(\DateTimeInterface $date): self
    {
        $this->date = $date;

        return $this;
    }

    public function getStartHour(): ?int
    {
        return $this->startHour;
    }

    public function setStartHour(int $startHour): self
    {
        $this->startHour = $startHour;

        return $this;
    }

    public function getPauseHour(): ?int
    {
        return $this->pauseHour;
    }

    public function setPauseHour(int $pauseHour): self
    {
        $this->pauseHour = $pauseHour;

        return $this;
    }

    public function getResumeHour(): ?int
    {
        return $this->resumeHour;
    }

    public function setResumeHour(int $resumeHour): self
    {
        $this->resumeHour = $resumeHour;

        return $this;
    }

    public function getEndHour(): ?int
    {
        return $this->endHour;
    }

    public function setEndHour(int $endHour): self
    {
        $this->endHour = $endHour;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function __toString(): string
    {
        return $this->id;
    }


}
