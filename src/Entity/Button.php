<?php

namespace App\Entity;

class Button
{
    public $name;
    public $icon;
    public $link;

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function setIcon(string $icon): self
    {
        $this->icon = $icon;

        return $this;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }
}
