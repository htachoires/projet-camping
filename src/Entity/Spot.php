<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Spot
 *
 * @ORM\Table(name="spot")
 * @ORM\Entity
 */
class Spot
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_reserved", type="boolean", nullable=false)
     * @Assert\NotNull()
     */
    private $isReserved;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_available", type="boolean", nullable=false)
     * @Assert\NotNull()
     */
    private $isAvailable;

    /**
     * @var string
     *
     * @ORM\Column(name="comment", type="string", length=65556, nullable=false)
     * @Assert\NotNull()
     * @Assert\NotBlank()
     * @Assert\Length(max="65556")
     */
    private $comment;

    /**
     * @var int
     *
     * @ORM\Column(name="size", type="integer", nullable=false)
     * @Assert\NotNull()
     * @Assert\Range(min="1")
     */
    private $size;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_shade", type="boolean", nullable=false)
     * @Assert\NotNull()
     */
    private $hasShade;

    /**
     * @var bool
     *
     * @ORM\Column(name="handicap_friendly", type="boolean", nullable=false)
     * @Assert\NotNull()
     */
    private $handicapFriendly;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_electricity", type="boolean", nullable=false)
     * @Assert\NotNull()
     */
    private $hasElectricity;

    /**
     * @var int
     *
     * @ORM\Column(name="max_capacity", type="integer", nullable=false)
     * @Assert\NotNull()
     * @Assert\Range(min="1")
     */
    private $maxCapacity;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_bathroom", type="boolean", nullable=false)
     * @Assert\NotNull()
     */
    private $hasBathroom;

    /**
     * @var bool
     *
     * @ORM\Column(name="has_toilet", type="boolean", nullable=false)
     * @Assert\NotNull()
     */
    private $hasToilet;

    /**
     * @var float
     *
     * @ORM\Column(name="longitude", type="float", nullable=false)
     * @Assert\NotNull()
     */
    private $longitude;

    /**
     * @var float
     *
     * @ORM\Column(name="latitude", type="float", nullable=false)
     * @Assert\NotNull()
     */
    private $latitude;

    public function __construct()
    {
        $this->setSize(15);
        $this->setMaxCapacity(5);
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsReserved(): ?bool
    {
        return $this->isReserved;
    }

    public function setIsReserved(bool $isReserved): self
    {
        $this->isReserved = $isReserved;

        return $this;
    }

    public function getIsAvailable(): ?bool
    {
        return $this->isAvailable;
    }

    public function setIsAvailable(bool $isAvailable): self
    {
        $this->isAvailable = $isAvailable;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getSize(): ?int
    {
        return $this->size;
    }

    public function setSize(int $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getHasShade(): ?bool
    {
        return $this->hasShade;
    }

    public function setHasShade(bool $hasShade): self
    {
        $this->hasShade = $hasShade;

        return $this;
    }

    public function getHandicapFriendly(): ?bool
    {
        return $this->handicapFriendly;
    }

    public function setHandicapFriendly(bool $handicapFriendly): self
    {
        $this->handicapFriendly = $handicapFriendly;

        return $this;
    }

    public function getHasElectricity(): ?bool
    {
        return $this->hasElectricity;
    }

    public function setHasElectricity(bool $hasElectricity): self
    {
        $this->hasElectricity = $hasElectricity;

        return $this;
    }

    public function getMaxCapacity(): ?int
    {
        return $this->maxCapacity;
    }

    public function setMaxCapacity(int $maxCapacity): self
    {
        $this->maxCapacity = $maxCapacity;

        return $this;
    }

    public function getHasBathroom(): ?bool
    {
        return $this->hasBathroom;
    }

    public function setHasBathroom(bool $hasBathroom): self
    {
        $this->hasBathroom = $hasBathroom;

        return $this;
    }

    public function getHasToilet(): ?bool
    {
        return $this->hasToilet;
    }

    public function setHasToilet(bool $hasToilet): self
    {
        $this->hasToilet = $hasToilet;

        return $this;
    }

    public function __toString(): string
    {
        return (string)$this->id;
    }

    public function setLongitude(float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    public function setLatitude(float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }


}
