<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * Person
 *
 * @ORM\Table(name="person")
 * @ORM\Entity
 * @UniqueEntity("email", message="This email is already taken")
 */
class Person
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=255, nullable=false)
     * @Assert\Regex(pattern="/\d/", match=false, message="Your firstname cannot contain a number")
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=255, nullable=false)
     * @Assert\Regex(pattern="/\d/", match=false, message="Your lastname cannot contain a number")
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     */
    private $lastName;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255, nullable=false)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255, nullable=false, unique=true)
     * @Assert\Email(message = "The email '{{ value }}' is not a valid email.")
     * @Assert\NotNull
     * @Assert\Length(max="255")
     */
    private $email;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="birth_date", type="datetime", nullable=false)
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    private $birthDate;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255, nullable=false)
     * @Assert\NotNull
     * @Assert\NotBlank
     * @Assert\Length(max="255")
     */
    private $phone;

    /**
     * @var int|null
     *
     * @ORM\Column(name="note", type="integer", nullable=true)
     * @Assert\Range(
     *      min = 0,
     *      max = 10,
     *      minMessage = "Min note is {{ limit }}",
     *      maxMessage = "Max note is {{ limit }}"
     * )
     */
    private $note;

    /**
     * @var string|null
     *
     * @ORM\Column(name="iban", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $iban;

    /**
     * @var string|null
     *
     * @ORM\Column(name="secu_num", type="string", length=255, nullable=true)
     * @Assert\Length(max="255")
     */
    private $secuNum;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getAddress(): ?string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getBirthDate(): ?\DateTimeInterface
    {
        return $this->birthDate;
    }

    public function setBirthDate(\DateTimeInterface $birthDate): self
    {
        $this->birthDate = $birthDate;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getNote(): ?int
    {
        return $this->note;
    }

    public function setNote(?int $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getIban(): ?string
    {
        return $this->iban;
    }

    public function setIban(?string $iban): self
    {
        $this->iban = $iban;

        return $this;
    }

    public function getSecuNum(): ?string
    {
        return $this->secuNum;
    }

    public function setSecuNum(?string $secuNum): self
    {
        $this->secuNum = $secuNum;

        return $this;
    }

    public function __toString(): String
    {
        return $this->email;
    }

}
