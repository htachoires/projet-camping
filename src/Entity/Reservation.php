<?php

namespace App\Entity;

use App\Validator\Constraints\NumberPerson;
use App\Validator\Constraints\StartEndDate;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Reservation
 *
 * @ORM\Table(name="reservation", indexes={
 *     @ORM\Index(name="personR_id", columns={"person_id"}),
 *     @ORM\Index(name="spotR_id", columns={"spot_id"})
 * })
 * @ORM\Entity
 * @NumberPerson(fieldName="personCount")
 * @StartEndDate()
 */
class Reservation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="start_date", type="datetime", nullable=false)
     * @Assert\NotNull()
     */
    private $startDate;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="end_date", type="datetime", nullable=false)
     * @Assert\NotNull()
     */
    private $endDate;

    /**
     * @var int
     *
     * @ORM\Column(name="price", type="integer", nullable=false)
     * @Assert\NotNull()
     * @Assert\Range(min="0")
     */
    private $price;

    /**
     * @var int
     *
     * @ORM\Column(name="person_count", type="integer", nullable=false)
     * @Assert\NotNull()
     * @Assert\Range(min="1")
     */
    private $personCount;

    /**
     * @var string|null
     *
     * @ORM\Column(name="comment", type="text", length=65535, nullable=true)
     */
    private $comment;

    /**
     * @var Person
     *
     * @ORM\ManyToOne(targetEntity="Person")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="person_id", referencedColumnName="id")
     * })
     * @Assert\NotNull()
     */
    private $person;

    /**
     * @var Spot
     *
     * @ORM\ManyToOne(targetEntity="Spot")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="spot_id", referencedColumnName="id")
     * })
     * @Assert\NotNull()
     */
    private $spot;

    public function __construct()
    {
        $this->startDate = date_time_set(new \DateTime("1 day"), $_ENV["ARRIVAL_HOUR_START_MIN"] ?? 8, 0, 0);
        $this->endDate = date_time_set(new \DateTime("3 day"), $_ENV["END_HOUR_START_MIN"] ?? 8, 0, 0);
        $this->setPersonCount(1);
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getStartDate(): ?\DateTimeInterface
    {
        return $this->startDate;
    }

    public function setStartDate(?\DateTimeInterface $startDate): self
    {
        $this->startDate = $startDate;

        return $this;
    }

    public function getEndDate(): ?\DateTimeInterface
    {
        return $this->endDate;
    }

    public function setEndDate(?\DateTimeInterface $endDate): self
    {
        $this->endDate = $endDate;

        return $this;
    }

    public function getPrice(): ?int
    {
        return $this->price;
    }

    public function setPrice(?int $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPersonCount(): ?int
    {
        return $this->personCount;
    }

    public function setPersonCount(?int $personCount): self
    {
        $this->personCount = $personCount;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getPerson(): ?Person
    {
        return $this->person;
    }

    public function setPerson(?Person $person): self
    {
        $this->person = $person;

        return $this;
    }

    public function getSpot(): ?Spot
    {
        return $this->spot;
    }

    public function setSpot(?Spot $spot): self
    {
        $this->spot = $spot;

        return $this;
    }

    public function __toString(): string
    {
        return $this->id;
    }

}
