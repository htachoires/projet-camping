<?php

namespace App\DataFixtures;

use App\Entity\Incident;
use App\Entity\Task;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class TaskFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $lorem = new LoremIpsum();
        $incidents = $manager->getRepository(Incident::class)->findAll();
        $NB_TASK = sizeof($incidents);
        for ($i = 0; $i < $NB_TASK; $i++) {
            $task = new Task();
            $task->setIncident($incidents[$i]);
            $dates = $this->generateReservationDate(date_timestamp_get($incidents[$i]->getDate()));
            $task->setStartDate(new \DateTime($dates[0]));
            $task->setEndDate(new \DateTime($dates[1]));
            $task->setState(rand(0, 2));
            $task->setDescription($lorem->words(5));
            $manager->persist($task);
        }
        $manager->flush();
    }

    /**
     * Generate two dates the second is after the first one
     *
     * @return array two element which are the start date and the end date
     */
    private function generateReservationDate($startDate): array
    {
        $dates = [];

        $dateStartTask = rand($startDate, strtotime("+3 days", $startDate));
        $hour = rand(6, 19);
        array_push($dates, date('Y-m-d ' . $hour . ':00', $dateStartTask));
        array_push($dates, date('Y-m-d ' . (rand($hour, 19) + 1) . ':00', $dateStartTask));
        return $dates;
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 7;
    }
}
