<?php

namespace App\DataFixtures;

use App\Entity\Person;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $NB_USER = $_ENV["NUMBER_OF_USER_FIXTURES_EXCLUDING_ADMIN"];
        $persons = $manager->getRepository(Person::class)->findBy([], null, $NB_USER);

        $adminPerson = $manager->getRepository(Person::class)->findBy(['email' => 'admin@admin.com']);
        for ($i = 0; $i < $NB_USER; $i++) {
            $person = $persons[$i];
            $user = new User();
            $user->setPerson($person);
            $user->setLogin($person->getFirstName()[0] . $person->getLastName());
            $user->setIsAdmin(0);
            $user->setPassword(password_hash("plop", PASSWORD_BCRYPT));
            $manager->persist($user);
        }
        $manager->persist($this->getAdminUser($adminPerson[0]));
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 8;
    }

    private function getAdminUser(Person $adminPerson): User
    {
        return (new User())
            ->setPerson($adminPerson)
            ->setIsAdmin(1)
            ->setLogin('admin')
            ->setPassword(password_hash("password", PASSWORD_BCRYPT));
    }
}
