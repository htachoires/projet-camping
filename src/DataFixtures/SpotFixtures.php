<?php

namespace App\DataFixtures;

use App\Entity\Spot;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class SpotFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $NB_SPOT = $_ENV["NUMBER_OF_SPOT_FIXTURES"];
        $lorem = new LoremIpsum();

        for ($i = 0; $i < $NB_SPOT; $i++) {
            $spot = new Spot();
            $spot->setComment($lorem->words(5));
            $spot->setHasBathroom(rand(0, 1));
            $spot->setHasElectricity(rand(0, 1));
            $spot->setHandicapFriendly(rand(0, 1));
            $spot->setHasShade(rand(0, 1));
            $spot->setHasToilet(rand(0, 1));
            $spot->setIsAvailable(1);
            $spot->setLongitude(10.0);
            $spot->setLatitude(10.0);
            $spot->setIsReserved(0);
            $spot->setMaxCapacity(rand(1, 10));
            $spot->setLatitude(44.896254);
            $spot->setLongitude(-0.5833892);
            $spot->setSize(rand(15, 120));
            $manager->persist($spot);
        }
        $manager->flush();
    }

    public function getOrder()
    {
        return 2;
    }
}
