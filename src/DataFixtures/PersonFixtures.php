<?php

namespace App\DataFixtures;

use App\Entity\Person;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class PersonFixtures extends Fixture implements OrderedFixtureInterface
{
    /**
     * @inheritDoc
     */
    public function load(ObjectManager $manager)
    {
        $names = [];
        $r = new RandomNameGenerator('associative_array');
        $NB_PERSON = $_ENV["NUMBER_OF_PERSON_FIXTURES"];

        try {
            $names = $r->generateNames($NB_PERSON);
        } catch (\Exception $e) {

        }
        for ($i = 0; $i < $NB_PERSON; $i++) {
            $person = new Person();
            $person->setLastName($names[$i]["last_name"]);
            $person->setAddress($this->generateNumber(2) . " " . $this->getTypeRue() . " des " . $names[$i]["last_name"]);
            $person->setBirthDate(new \DateTime(date("Y-m-d", rand(strtotime("-80 years"), strtotime("-16 years")))));
            $person->setEmail($names[$i]["first_name"] . "." . $names[$i]["last_name"] . "@example.com");
            $person->setPhone($this->getPhoneNumber());
            $person->setFirstName($names[$i]["first_name"]);
            $person->setNote($this->getNote());
            $manager->persist($person);
        }
        $manager->persist($this->getAdminPerson());
        $manager->flush();
    }

    private function getTypeRue(): string
    {
        $type = ["Rue", "Avenue", "Allé", "Place", "Résidence"];
        return $type[array_rand($type)];
    }

    private function getPhoneNumber(): string
    {
        return "0" . rand(6, 7) . $this->generateNumber(7);
    }

    private function generateNumber($nbDigits): string
    {
        $result = "";
        $count = 0;
        while ($count < $nbDigits) {
            $random_digit = mt_rand(0, 9);
            $result .= $random_digit;
            $count++;
        }
        return $result;
    }

    function getNote(): ?int
    {
        return rand(0, 4) == 3 ? null : rand(0, 10);
    }

    public function getOrder()
    {
        return 1;
    }

    private function getAdminPerson()
    {
        return (new Person())
            ->setFirstName("admin")
            ->setLastName("admin")
            ->setPhone("123456789")
            ->setAddress("33 administrator street")
            ->setEmail("admin@admin.com")
            ->setBirthDate(new \DateTime("-1000 years"));
    }
}
