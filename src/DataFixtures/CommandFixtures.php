<?php

namespace App\DataFixtures;

use App\Entity\Command;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class CommandFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $lorem = new LoremIpsum();
        $NB_COMMAND = 500;

        for ($i = 0; $i < $NB_COMMAND; $i++) {
            $command = new Command();
            $command->setTitle($lorem->words(5))
                ->setDescription($lorem->sentence())
                ->setSupplier($lorem->words(5))
                ->setCommandDate(new \DateTime(date('Y-m-d H:i:s', strtotime("now"))))
                ->setDeliveryDate(new \DateTime(date('Y-m-d ' . (rand(6, 20)) . ':00', rand(strtotime("+5 days"), strtotime("+2 week")))));
            $manager->persist($command);
        }
        $manager->flush();
    }

    /**
     * Get the order of this fixture
     *
     * @return int
     */
    public function getOrder()
    {
        return 9;
    }
}
