<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Entity\WorkPeriod;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class WorkPeriodFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $NB_WORK_PERIOD_PER_USER = $_ENV["NUMBER_MAX_OF_WORK_PERIOD_PER_USER_FIXTURES"];
        $users = $manager->getRepository(User::class)->findAll();
        foreach ($users as $user) {
            $date = strtotime("now");
            for ($i = 0; $i < $NB_WORK_PERIOD_PER_USER; $i++) {
                $date = $this->getDateOfWork($date);
                if (rand(0, 6) < 6) {
                    $workPeriod = new WorkPeriod();
                    $workPeriod->setPerson($user->getPerson());
                    $workPeriod->setDate(new \DateTime(date('Y-m-d', $date)));
                    $hours = $this->generateDayOfWork();
                    $workPeriod->setStartHour($hours[0]);
                    $workPeriod->setPauseHour($hours[1]);
                    $workPeriod->setResumeHour($hours[2]);
                    $workPeriod->setEndHour($hours[3]);
                    $manager->persist($workPeriod);
                }
            }
        }
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 11;
    }

    private function generateDayOfWork(): array
    {
        $hours = [];
        $startHour = rand(0, 1) == 0 ? rand(6, 8) : rand(12, 14);
        $pauseHour = $startHour + 4;
        $resumeHour = $pauseHour + 1;
        $endHour = $startHour + 7;
        array_push($hours, $startHour, $pauseHour, $resumeHour, $endHour);
        return $hours;
    }

    private function getDateOfWork(int $date)
    {
        return strtotime("+1 day", $date);
    }
}
