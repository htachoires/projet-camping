<?php

namespace App\DataFixtures;

use App\Entity\ConfigVariable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ConfigVariableFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $names = ["ARRIVAL_HOUR_START_MIN", "ARRIVAL_HOUR_START_MAX", "END_HOUR_START_MIN", "END_HOUR_START_MAX", "START_WORK_HOUR", "END_WORK_HOUR"];
        $values = [$_ENV["ARRIVAL_HOUR_START_MIN"] ?? 8, $_ENV["ARRIVAL_HOUR_START_MAX"] ?? 11,
            $_ENV["END_HOUR_START_MIN"] ?? 8, $_ENV["END_HOUR_START_MAX"] ?? 11,
            $_ENV["START_WORK_HOUR"] ?? 6, $_ENV["END_WORK_HOUR"] ?? 21
        ];

        for ($i = 0; $i < count($names); $i++) {
            $variable = new ConfigVariable();
            $variable->setName($names[$i])
                ->setValue($values[$i]);
            $manager->persist($variable);
        }
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 14;
    }
}
