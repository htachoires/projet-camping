<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class CategoryFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $lorem = new LoremIpsum();
        $NB_CATEGORY = 40;
        $names = $lorem->wordsArray($NB_CATEGORY);
        for ($i = 0; $i < $NB_CATEGORY; $i++) {
            $category = new Category();
            $category->setName($names[$i]);
            $manager->persist($category);
        }

        $manager->flush();
    }

    public function getOrder()
    {
        return 3;
    }
}
