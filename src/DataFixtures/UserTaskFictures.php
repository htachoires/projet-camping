<?php

namespace App\DataFixtures;

use App\Entity\Task;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class UserTaskFictures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $NB_TASK_PER_USER = $_ENV["NUMBER_OF_TASK_PER_USER_FIXTURES"];
        $users = $manager->getRepository(User::class)->findAll();
        $tasks = $manager->getRepository(Task::class)->findAll();
        foreach ($users as $user) {
            for ($i = 0; $i < $NB_TASK_PER_USER; $i++) {
                $user->addTask($tasks[array_rand($tasks)]);
            }
        }
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 13;
    }
}
