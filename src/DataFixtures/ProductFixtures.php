<?php

namespace App\DataFixtures;

use App\Entity\Category;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class ProductFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $lorem = new LoremIpsum();
        $NB_PRODUCT = $_ENV["NUMBER_OF_PRODUCT_FIXTURES"];
        $names = $lorem->wordsArray($NB_PRODUCT);
        $category = $manager->getRepository(Category::class)->findBy([], null, $NB_PRODUCT);
        for ($i = 0; $i < $NB_PRODUCT; $i++) {
            $product = new Product();
            $product->setCategory($category[rand(0, sizeof($category) - 1)]);
            $product->setName($names[$i]);
            $product->setPrice(rand(1, 450));
            $product->setQuantity(rand(0, 5000));
            $manager->persist($product);
        }

        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 4;
    }
}
