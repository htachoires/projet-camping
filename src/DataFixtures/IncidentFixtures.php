<?php

namespace App\DataFixtures;

use App\Entity\Incident;
use App\Entity\Spot;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class IncidentFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $lorem = new LoremIpsum();
        $NB_INCIDENT = $_ENV["NUMBER_OF_INCIDENT_FIXTURES"];
        $spots = $manager->getRepository(Spot::class)->findAll();

        for ($i = 0; $i < $NB_INCIDENT; $i++) {
            $incident = new Incident();
            $incident->setComment($lorem->sentence());
            $incident->setDate(new \DateTime(date('Y-m-d ' . rand(6, 19) . ':i', rand(strtotime("now"), strtotime("+1 week")))));
            $incident->setSpot($spots[rand(0, sizeof($spots) - 1)]);
            $incident->setImportant(rand(1, 10));
            $manager->persist($incident);
        }
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 6;
    }
}
