<?php

namespace App\DataFixtures;

use App\Entity\Person;
use App\Entity\Reservation;
use App\Entity\Spot;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class ReservationFixtures extends Fixture implements OrderedFixtureInterface
{

    private $ARRIVAL_HOUR_START_MIN;
    private $ARRIVAL_HOUR_START_MAX;

    private $END_HOUR_START_MIN;
    private $END_HOUR_START_MAX;

    /**
     * @var DateTime
     */
    private $actualReservation;

    public function __construct()
    {
        $this->ARRIVAL_HOUR_START_MIN = $_ENV['ARRIVAL_HOUR_START_MIN'] ?? 8;
        $this->ARRIVAL_HOUR_START_MAX = $_ENV['ARRIVAL_HOUR_START_MAX'] ?? 11;

        $this->END_HOUR_START_MIN = $_ENV['END_HOUR_START_MIN'] ?? 8;
        $this->END_HOUR_START_MAX = $_ENV['END_HOUR_START_MAX'] ?? 11;
    }

    public function load(ObjectManager $manager)
    {
        srand(123456789);
        $lorem = new LoremIpsum();
        $MAX_NB_RESERVATION = 150;
        $persons = $manager->getRepository(Person::class)->findAll();
        $spots = $manager->getRepository(Spot::class)->findAll();
        foreach ($spots as $spot) {
            $this->setActualStartReservationDate("-1 year");
            for ($i = 0; $i < rand(0, $MAX_NB_RESERVATION); $i++) {
                $reservation = new Reservation();
                $reservation->setPerson($persons[array_rand($persons)]);
                $reservation->setSpot($spot);
                $reservation->setPrice(rand(1200, 15000));
                $reservation->setComment($lorem->sentence());

                $dates = $this->generateReservationDate();
                $reservation->setStartDate($dates[0]);
                $reservation->setEndDate($dates[1]);

                $reservation->setPersonCount(rand(1, $spot->getMaxCapacity()));

                /*set is reserved if the date is later than now*/
                if (!$spot->getIsReserved()) {
                    $spot->setIsReserved($dates[0] > new DateTime("now"));
                    $spot->setIsAvailable($spot->getIsReserved() == 1 ? 0 : 1);
                }

                $manager->persist($reservation);
            }
        }
        $manager->flush();
    }


    /**
     * Generate two dates the second is after the first one
     *
     * @return array two element which are the start date and the end date
     * @throws \Exception
     */
    private function generateReservationDate(): array
    {
        $dates = [];

        $min = strtotime("1 day", $this->actualReservation->getTimestamp());
        $max = strtotime("2 week", $this->actualReservation->getTimestamp());

        $startDateTimestamp = rand($min, $max);
        $startDate = date('Y-m-d ' . rand($this->ARRIVAL_HOUR_START_MIN, $this->ARRIVAL_HOUR_START_MAX) . ':00', $startDateTimestamp);
        $endDate = date('Y-m-d ' . rand($this->END_HOUR_START_MIN, $this->END_HOUR_START_MAX) . ':00', rand(strtotime("2 day", $startDateTimestamp), strtotime("2 week", $startDateTimestamp)));

        array_push($dates, new DateTime($startDate));
        array_push($dates, new DateTime($endDate));

        $this->setActualStartReservationDate($endDate);

        return $dates;
    }

    public function setActualStartReservationDate(string $date)
    {
        $this->actualReservation = new DateTime($date);
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 5;
    }
}
