<?php

namespace App\DataFixtures;

use App\Entity\History;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use joshtronic\LoremIpsum;

class HistoryFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $lorem = new LoremIpsum();
        $NB_HISTORY = 500;
        $users = $manager->getRepository(User::class)->findAll();
        for ($i = 0; $i < $NB_HISTORY; $i++) {
            $history = new History();
            $history->setDescription($lorem->words(5));
            $history->setDate(new \DateTime(date("Y-m-d " . rand(6, 19) . ":i:s", rand(strtotime("-1 month"), strtotime("now")))));
            $history->setUser($users[array_rand($users)]);
            $manager->persist($history);
        }

        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 10;
    }
}
