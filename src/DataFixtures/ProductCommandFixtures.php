<?php

namespace App\DataFixtures;

use App\Entity\Command;
use App\Entity\Product;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Persistence\ObjectManager;

class ProductCommandFixtures extends Fixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $NB_PRODUCT_PER_COMMAND = $_ENV["NUMBER_OF_PRODUCT_PER_COMMAND_FIXTURES"];
        $commands = $manager->getRepository(Command::class)->findAll();
        $products = $manager->getRepository(Product::class)->findAll();
        foreach ($commands as $command) {
            for ($i = 0; $i < $NB_PRODUCT_PER_COMMAND; $i++) {
                $command->addProduct($products[array_rand($products)]);
            }
        }
        $manager->flush();
    }

    /**
     * @inheritDoc
     */
    public function getOrder()
    {
        return 12;
    }
}
