<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 */
class NumberPerson extends Constraint
{
    public $message = "Le spot peut contenir au maximum {{ maxPerson }} personnes, vous en avez entrée {{ nbPerson }}.";
    public $fieldName;

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    /**
     * @return mixed
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }


}