<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 * @Target({"CLASS", "ANNOTATION"})
 */
class StartEndDate extends Constraint
{
    public $message = "Une réservation est déjà prévue du {{ startDate }} au {{ endDate }}. Impossible de créer une réservation à partir du {{ date }}.";
    public $messageEndTooLate = "Une réservation est déjà prévue du {{ startDate }} au {{ endDate }}. Vous ne pouvez pas la faire terminer le {{ date }}.";
    public $messagePast = "La date de début de la réservation doit etre prise à partir du {{ minNextDate }}. Le {{ date }} est une date incorrecte.";
    public $messageHourStart = "Le début d'une réservation ne peut être prise qu'entre {{ minHour }}h et {{ maxHour }}h. {{ date }}h est invalide.";
    public $messageEndDateBeforeStartDate = "La date de fin de la réservation doit être au minimum le {{ minEndDate }}. Le {{ date }} est une date incorrecte.";
    public $messageHourEnd = "Une reservation ne peut se terminer qu'entre {{ minHour }}h et {{ maxHour }}h. {{ date }}h est invalide.";
    public $date;

    /**
     * {@inheritdoc}
     */
    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }
}