<?php


namespace App\Validator\Constraints;

use App\Entity\ConfigVariable;
use App\Entity\Reservation;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use Symfony\Component\Validator\Exception\UnexpectedValueException;

class StartEndDateValidator extends ConstraintValidator
{

    private $entityManager;

    private $ARRIVAL_HOUR_START_MIN;
    private $ARRIVAL_HOUR_START_MAX;

    private $END_HOUR_START_MIN;
    private $END_HOUR_START_MAX;

    /**
     * @var Reservation[]
     */
    private $reservations;

    public function __construct(EntityManagerInterface $entityManagerInterface)
    {
        $this->entityManager = $entityManagerInterface;
        $repo = $entityManagerInterface->getRepository(ConfigVariable::class);

        $this->ARRIVAL_HOUR_START_MIN = $repo->findBy(["name" => "ARRIVAL_HOUR_START_MIN"])[0]->getValue();
        $this->ARRIVAL_HOUR_START_MAX = $repo->findBy(["name" => "ARRIVAL_HOUR_START_MAX"])[0]->getValue();
        $this->END_HOUR_START_MIN = $repo->findBy(["name" => "END_HOUR_START_MIN"])[0]->getValue();
        $this->END_HOUR_START_MAX = $repo->findBy(["name" => "END_HOUR_START_MAX"])[0]->getValue();
    }

    /**
     * @inheritDoc
     * @throws Exception
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof StartEndDate) {
            throw new UnexpectedTypeException($constraint, StartEndDate::class);
        }

        if ($value === null) {
            return;
        }

        if (!$value instanceof Reservation) {
            throw new UnexpectedValueException($value, 'App\Entity\Reservation');
        }

        if ($value->getStartDate() === null || $value->getEndDate() === null || $value->getSpot() === null) {
            return;
        }

        if ($this->validateStartDate($value, $constraint)) {
            $this->validateEndDate($value, $constraint);
        }
    }

    /**
     * @param Reservation $value
     * @param Constraint $constraint
     * @return bool|void
     * @throws Exception
     */
    private function validateStartDate($value, Constraint $constraint)
    {

        $minNextDate = date_time_set(new \DateTime("1 day"), $this->ARRIVAL_HOUR_START_MIN, 0, 0);
        $newStartDate = $value->getStartDate();
        if ($newStartDate < $minNextDate) {
            $this->context->buildViolation($constraint->messagePast)
                ->setParameters([
                    '{{ minNextDate }}' => $minNextDate->format('d-m-Y à H:i'),
                    '{{ date }}' => $newStartDate->format('d-m-Y à H:i'),
                ])
                ->addViolation();
            return false;
        }


        $hourStartDate = intval($newStartDate->format('H'));

        if ($hourStartDate < $this->ARRIVAL_HOUR_START_MIN || $hourStartDate > $this->ARRIVAL_HOUR_START_MAX) {
            $this->context->buildViolation($constraint->messageHourStart)
                ->setParameters([
                    '{{ minHour }}' => $this->ARRIVAL_HOUR_START_MIN,
                    '{{ maxHour }}' => $this->ARRIVAL_HOUR_START_MAX,
                    '{{ date }}' => $newStartDate->format('H'),
                ])
                ->addViolation();
            return false;
        }

        $this->reservations = $this->entityManager->getRepository(Reservation::class)->findBy(['spot' => $value->getSpot()->getId()]);

        $invalidStartDate = false;

        $startDate = null;
        $endDate = null;

        foreach ($this->reservations as $reservation) {
            if ($reservation !== $value) {
                $startDate = $reservation->getStartDate();
                $endDate = date_time_set(new \DateTime(date('d-m-Y H:i', strtotime("1 day", $reservation->getEndDate()->getTimestamp()))), $this->END_HOUR_START_MIN, 0, 0);
                if (($newStartDate >= $startDate && $newStartDate < $endDate)) {
                    $invalidStartDate = true;
                    break;
                }
            }
        }

        if ($invalidStartDate) {
            $this->context->buildViolation($constraint->message)
                ->setParameters([
                    '{{ startDate }}' => $startDate->format('d-m-Y'),
                    '{{ endDate }}' => $endDate->format('d-m-Y'),
                    '{{ date }}' => $value->getStartDate()->format('d-m-Y à H:i'),
                ])
                ->addViolation();
            return false;
        }

        return true;
    }

    /**
     * @param Reservation $value
     * @param Constraint $constraint
     * @return bool
     * @throws Exception
     */
    private function validateEndDate($value, Constraint $constraint)
    {
        $newStartDate = $value->getStartDate();

        $minEndDate = date_time_set(
            new \DateTime(
                date('Y-m-d H:00',
                    strtotime("1 day", $newStartDate->getTimestamp())
                )
            ), $this->END_HOUR_START_MIN, 0, 0);
        $newEndDate = $value->getEndDate();

        if ($newEndDate < $minEndDate) {
            $this->context->buildViolation($constraint->messageEndDateBeforeStartDate)
                ->setParameters([
                    '{{ minEndDate }}' => $minEndDate->format('d-m-Y à H:i'),
                    '{{ date }}' => $newEndDate->format('d-m-Y à H:i'),
                ])
                ->addViolation();
            return false;
        }


        $hourEndDate = intval($newEndDate->format('H'));

        if ($hourEndDate < $this->END_HOUR_START_MIN || $hourEndDate > $this->END_HOUR_START_MAX) {
            $this->context->buildViolation($constraint->messageHourEnd)
                ->setParameters([
                    '{{ minHour }}' => $this->END_HOUR_START_MIN,
                    '{{ maxHour }}' => $this->END_HOUR_START_MAX,
                    '{{ date }}' => $newEndDate->format('H'),
                ])
                ->addViolation();
            return false;
        }

        $invalidEndDate = false;

        $startDate = null;
        $endDate = null;

        foreach ($this->reservations as $reservation) {
            if ($reservation !== $value) {
                $startDate = date_time_set(new \DateTime(date('d-m-Y H:i', strtotime("-1 day", $reservation->getStartDate()->getTimestamp()))), $this->ARRIVAL_HOUR_START_MAX, 0, 0);
                $endDate = $reservation->getEndDate();
                if ($newEndDate > $startDate && $newStartDate <= $endDate) {
                    $invalidEndDate = true;
                    break;
                }
            }
        }

        if ($invalidEndDate) {
            $this->context->buildViolation($constraint->messageEndTooLate)
                ->setParameters([
                    '{{ startDate }}' => $startDate->format('d-m-Y'),
                    '{{ endDate }}' => $endDate->format('d-m-Y'),
                    '{{ date }}' => $value->getEndDate()->format('d-m-Y à H:i'),
                ])
                ->addViolation();
            return false;
        }


        return true;
    }
}