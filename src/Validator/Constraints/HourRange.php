<?php


namespace App\Validator\Constraints;


use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\Exception\MissingOptionsException;

/**
 * @Annotation
 */
class HourRange extends Constraint
{

    static $MIN_HOUR = 0;
    static $MAX_HOUR = 23;

    public $messageMin = "L'heure minimale valide est {{ min }}h00.";
    public $messageMax = "L'heure maximale valide est {{ max }}h00.";

    public $messageError = "Impossible de trouver la variable \"{{ variable }}\", vérifiez votre configuration.";

    public $min;
    public $max;

    public function __construct($options = null)
    {
        parent::__construct($options);

        if (null === $this->min || null === $this->max) {
            throw new MissingOptionsException('Either option "min", "max" must be given for constraint ', ['min', 'max']);
        }
    }


}