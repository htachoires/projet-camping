<?php


namespace App\Validator\Constraints;


use App\Entity\ConfigVariable;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class HourRangeValidator extends ConstraintValidator
{
    /**
     * @var int
     */
    private $max;

    /**
     * @var int
     */
    private $min;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->em = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function validate($value, Constraint $constraint)
    {
        if (!$constraint instanceof HourRange) {
            throw new UnexpectedTypeException($constraint, HourRange::class);
        }

        if ($value === null) return;

        if (!$value instanceof \DateTime) {
            throw new UnexpectedTypeException($value, '\Datetime');
        }

        $repo = $this->em->getRepository(ConfigVariable::class);

        if (is_string($constraint->min)) {
            $min = $repo->findBy(["name" => $constraint->min]);
            if (count($min) === 0) {
                $this->context->buildViolation($constraint->messageError)
                    ->setParameter('{{ variable }}', $constraint->min)
                    ->addViolation();
                return;
            }
            $min = $min[0]->getValue();
        } else {
            $min = $constraint->min;
        }

        if (is_string($constraint->max)) {
            $max = $repo->findBy(["name" => $constraint->max]);
            if (count($max) === 0) {
                $this->context->buildViolation($constraint->messageError)
                    ->setParameter('{{ variable }}', $constraint->max)
                    ->addViolation();
                return;
            }
            $max = $max[0]->getValue();
        } else {
            $max = $constraint->max;
        }

        if ($min < HourRange::$MIN_HOUR) {
            throw new InvalidArgumentException("min cannot be negative");
        }

        if ($max > HourRange::$MAX_HOUR) {
            throw new InvalidArgumentException("max cannot be greater than 23");
        }

        if (max($max, $min) != $max) {
            throw new InvalidArgumentException("max must be greater than min value");
        }

        $hour = (int)$value->format("H");
        if ($hour > $max) {
            $this->context->buildViolation($constraint->messageMax)
                ->setParameter('{{ max }}', $max)
                ->addViolation();
        }

        if ($hour < $min) {
            $this->context->buildViolation($constraint->messageMin)
                ->setParameter('{{ min }}', $min)
                ->addViolation();
        }

    }
}