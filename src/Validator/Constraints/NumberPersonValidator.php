<?php


namespace App\Validator\Constraints;


use App\Entity\Reservation;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class NumberPersonValidator extends ConstraintValidator
{

    /**
     * @inheritDoc
     */
    public function validate($value, Constraint $constraint)
    {

        if (!$constraint instanceof NumberPerson) {
            throw new UnexpectedTypeException($constraint, NumberPerson::class);
        }

        if ($value === null) {
            return;
        }

        if ($value->getSpot() === null || $value->getSpot()->getMaxCapacity() === null || $value->getPersonCount() === null) {
            return;
        }

        if (!$value instanceof Reservation) {
            throw new UnexpectedTypeException($value, 'App\Entity\Reservation');
        }

        $maxValue = $value->getSpot()->getMaxCapacity();
        $actualValue = $value->getPersonCount();
        if ($actualValue > $maxValue) {

            $fieldName = $constraint->getFieldName();
            $fieldName = $fieldName === null ? "" : $fieldName;
            $this->context->buildViolation($constraint->message)
                ->setInvalidValue($fieldName)
                ->atPath($fieldName)
                ->setParameters([
                    '{{ maxPerson }}' => $maxValue,
                    '{{ nbPerson }}' => $actualValue
                ])
                ->addViolation();
        }
    }
}