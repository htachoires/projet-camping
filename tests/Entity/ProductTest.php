<?php

namespace App\Tests\Entity;

use App\DataFixtures\CategoryFixtures;
use App\DataFixtures\ProductFixtures;
use App\Entity\Category;
use App\Entity\Product;
use App\Repository\ProductRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ProductTest extends KernelTestCase
{
    use FixturesTrait;

    public function testCount()
    {
        self::bootKernel();
        $this->loadFixtures([CategoryFixtures::class, ProductFixtures::class]);
        $products = self::$container->get(ProductRepository::class)->count([]);
        $this->assertEquals(500, $products);
    }

    public function testProductValid()
    {
        $this->validate($this->getProduct(), 0);
    }

    public function testProductInvalid()
    {
        $this->validate(new Product(), 5);
    }

    private function validate($person, $number)
    {
        self::bootKernel();
        $error = self::$container->get("validator")->validate($person);
        $this->assertCount($number, $error);
    }

    private function getProduct(): Product
    {
        return (new Product())
            ->setPrice(100)
            ->setQuantity(100)
            ->setName("toto")
            ->setCategory(new Category())
            ->setSoldTo(new \DateTime("3 day"));
    }
}