<?php

namespace App\Tests;

use App\DataFixtures\PersonFixtures;
use App\Entity\Person;
use App\Repository\PersonRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class PersonTest extends KernelTestCase
{

    use FixturesTrait;

    public function testCount()
    {
        self::bootKernel();
        $this->loadFixtures([PersonFixtures::class]);
        $issues = self::$container->get(PersonRepository::class)->count([]);
        $this->assertEquals(501, $issues);
    }


    public function testPersonValid()
    {
        $this->validate($this->getPerson(), 0);
    }

    public function testPersonNamesWithNumber()
    {
        $this->validate(
            $this->getPerson()
                ->setFirstName("toto33")
                ->setLastName("tata33")
            , 2);
    }

    public function testPersonNamesWithBlank()
    {
        $this->validate(
            $this->getPerson()
                ->setFirstName("")
                ->setLastName("")
            , 2);
    }

    private function validate($person, $number)
    {
        self::bootKernel();
        $error = self::$container->get("validator")->validate($person);
        $this->assertCount($number, $error);
    }

    private function getPerson(): Person
    {
        return (new Person())
            ->setFirstName("toto")
            ->setLastName("tata")
            ->setPhone("0123456789")
            ->setAddress("33 rue des toto")
            ->setEmail("toto@exampe.com")
            ->setBirthDate(new \DateTime("-18 years"));
    }

}