<?php

namespace App\Tests\Entity;

use App\DataFixtures\HistoryFixtures;
use App\DataFixtures\PersonFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\History;
use App\Entity\User;
use App\Repository\HistoryRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class HistoryTest extends KernelTestCase
{
    use FixturesTrait;

    public function testCount()
    {
        self::bootKernel();
        $this->loadFixtures([PersonFixtures::class, UserFixtures::class, HistoryFixtures::class]);
        $histories = self::$container->get(HistoryRepository::class)->count([]);
        $this->assertEquals(500, $histories);
    }

    public function testHistoryValid()
    {
        $this->validate($this->getHistory(), 0);
    }

    public function testHistoryUserNull()
    {
        $this->validate(($this->getHistory())->setUser(null), 1);
    }

    public function testHistoryDescriptionBlank()
    {
        $this->validate(($this->getHistory())->setDescription(""), 1);
    }

    public function testHistoryDescriptionTooShort()
    {
        $this->validate(($this->getHistory())->setDescription(""), 1);
    }

    public function testHistoryTooLong()
    {
        $this->validate(($this->getHistory())->setDescription(str_repeat("a", 256)), 1);
    }

    private function getHistory()
    {
        return (new History())->setDescription("toto")->setUser(new User());
    }

    private function validate($history, $number)
    {
        self::bootKernel();
        $error = self::$container->get("validator")->validate($history);
        $this->assertCount($number, $error, $error);
    }
}