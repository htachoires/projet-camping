<?php

namespace App\Tests\Entity;

use App\DataFixtures\CategoryFixtures;
use App\Entity\Category;
use App\Repository\CategoryRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CategoryTest extends KernelTestCase
{

    use FixturesTrait;

    public function testCount()
    {
        self::bootKernel();
        $this->loadFixtures([CategoryFixtures::class]);
        $categories = self::$container->get(CategoryRepository::class)->count([]);
        $this->assertEquals(40, $categories);
    }

    public function testCategoryValid()
    {
        $this->validate($this->getCategory(), 0);
    }

    public function testCategoryValidName()
    {
        $this->validate(($this->getCategory())->setName("d6a4zaz54 dzadza zza"), 0);
    }

    public function testCategoryNameTooShort()
    {
        $this->validate(($this->getCategory())->setName("zza"), 1);
    }

    public function testCategoryNameBlank()
    {
        $this->validate(($this->getCategory())->setName(""), 2);
    }

    public function testCategoryTooLong()
    {
        $this->validate(($this->getCategory())->setName(str_repeat("a", 256)), 1);
    }

    private function getCategory()
    {
        return (new Category())->setName("toto");
    }

    private function validate($category, $number)
    {
        self::bootKernel();
        $error = self::$container->get("validator")->validate($category);
        $this->assertCount($number, $error, $error);
    }
}