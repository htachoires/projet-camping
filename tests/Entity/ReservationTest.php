<?php

namespace App\Tests\Entity;

use App\DataFixtures\ConfigVariableFixtures;
use App\DataFixtures\PersonFixtures;
use App\DataFixtures\ReservationFixtures;
use App\DataFixtures\SpotFixtures;
use App\Entity\Person;
use App\Entity\Reservation;
use App\Entity\Spot;
use App\Repository\ReservationRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class ReservationTest extends KernelTestCase
{

    use FixturesTrait;

    private $maxCapacity = 10;

    public function testCount()
    {
        self::bootKernel();
        $this->loadFixtures([ConfigVariableFixtures::class, SpotFixtures::class, PersonFixtures::class, ReservationFixtures::class]);
        $reservations = self::$container->get(ReservationRepository::class)->count([]);
        $this->assertEquals(329, $reservations);
    }

    public function testReservationAllFieldNull()
    {
        $reservation = new Reservation();
        $this->validate($reservation, 3, false);
    }

    public function testReservationValid()
    {
        $this->validate($this->getReservation(), 0, false);
    }

    public function testReservationPersonNull()
    {
        $reservation = $this->getReservation();
        $reservation->setPerson(null);
        $this->validate($reservation, 1, false);
    }

    public function testReservationEndDateNull()
    {
        $reservation = $this->getReservation();
        $reservation->setEndDate(null);
        $this->validate($reservation, 1, false);
    }

    public function testReservationStartDateNull()
    {
        $reservation = $this->getReservation();
        $reservation->setStartDate(null);
        $this->validate($reservation, 1, false);
    }

    public function testReservationPersonCountNull()
    {
        $reservation = $this->getReservation();
        $reservation->setPersonCount(null);
        $this->validate($reservation, 1, false);
    }

    public function testReservationPersonCountTooHigh()
    {
        $reservation = $this->getReservation();
        $reservation->setPersonCount($this->maxCapacity + 1);
        $this->validate($reservation, 1, false);
    }

    public function testReservationPersonCountZero()
    {
        $reservation = $this->getReservation();
        $reservation->setPersonCount(0);
        $this->validate($reservation, 1, false);
    }

    public function testReservationPriceNull()
    {
        $reservation = $this->getReservation();
        $reservation->setPrice(null);
        $this->validate($reservation, 1, false);
    }

    private function getReservation()
    {
        $reservation = (new Reservation())
            ->setPrice(10)
            ->setPersonCount($this->maxCapacity)
            ->setStartDate(date_time_set(new \DateTime("2 day"), $_ENV['ARRIVAL_HOUR_START_MIN'], 0, 0))
            ->setEndDate(date_time_set(new \DateTime("4 day"), $_ENV['END_HOUR_START_MIN'], 0, 0))
            ->setPerson(new Person())
            ->setSpot((new Spot())->setMaxCapacity($this->maxCapacity));

        $this->validate($reservation, 0, true);

        return $reservation;
    }

    /**
     * @param Reservation $reservation the reservation to validate
     * @param int $expected number of errors that should be throw
     * @param bool $displayErrors display errors if true else nothing
     */
    private function validate(Reservation $reservation, int $expected, bool $displayErrors)
    {
        self::bootKernel();
        $validator = self::$container->get('validator');
        $errors = $validator->validate($reservation);
        if ($displayErrors)
            $this->assertCount($expected, $errors, $errors);
        else
            $this->assertCount($expected, $errors);
    }
}