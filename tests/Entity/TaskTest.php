<?php

namespace App\Tests\Entity;

use App\DataFixtures\ConfigVariableFixtures;
use App\DataFixtures\IncidentFixtures;
use App\DataFixtures\TaskFixtures;
use App\DataFixtures\SpotFixtures;
use App\Entity\Incident;
use App\Entity\Task;
use App\Repository\TaskRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class TaskTest extends KernelTestCase
{
    use FixturesTrait;

    public function testCount()
    {
        self::bootKernel();
        $this->loadFixtures([ConfigVariableFixtures::class, SpotFixtures::class, IncidentFixtures::class, TaskFixtures::class]);
        $tasks = self::$container->get(TaskRepository::class)->count([]);
        $this->assertEquals(500, $tasks);
    }

    /**
     * @param Task $Task the Task to validate
     * @param int $expected number of errors that should be throw
     * @param bool $displayErrors display errors if true else nothing
     */
    private function validate(?Task $Task, int $expected, bool $displayErrors)
    {
        self::bootKernel();
        $validator = self::$container->get('validator');
        $errors = $validator->validate($Task);
        if ($displayErrors)
            $this->assertCount($expected, $errors, $errors);
        else
            $this->assertCount($expected, $errors);
    }

    public function testTaskValid()
    {
        $this->validate($this->getTask(), 0, false);
    }

    public function testTaskInvalid()
    {
        $this->validate((new Task())->setStartDate(null)->setEndDate(null)->setState(null), 6, false);
    }

    public function testTaskInvalidStartStartDateHour()
    {
        $task = $this->getTask();
        $task->setStartDate(date_time_set(new \DateTime(), $_ENV["START_WORK_HOUR"] - 1, 0, 0));
        $this->validate($task, 1, false);
    }

    public function testTaskInvalidStartEndDateHour()
    {
        $task = $this->getTask();
        $task->setStartDate(date_time_set(new \DateTime(), $_ENV["END_WORK_HOUR"] + 1, 0, 0));
        $this->validate($task, 1, false);
    }

    public function testTaskInvalidEndStartDateHour()
    {
        $task = $this->getTask();
        $task->setEndDate(date_time_set(new \DateTime(), $_ENV["START_WORK_HOUR"] - 1, 0, 0));
        $this->validate($task, 1, false);
    }

    public function testTaskInvalidEndEndDateHour()
    {
        $task = $this->getTask();
        $task->setEndDate(date_time_set(new \DateTime(), $_ENV["END_WORK_HOUR"] + 1, 0, 0));
        $this->validate($task, 1, false);
    }

    /**
     * @return task Valid Task to use
     */
    private function getTask()
    {
        $task = (new Task())
            ->setDescription("Hello World !")
            ->setStartDate(date_time_set(new \DateTime(), $_ENV["START_WORK_HOUR"], 0, 0))
            ->setEndDate(date_time_set(new \DateTime(), $_ENV["END_WORK_HOUR"], 0, 0))
            ->setIncident(new Incident());

        $this->validate($task, 0, true);

        return $task;
    }
}