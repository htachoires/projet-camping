<?php

namespace App\Tests\Entity;

use App\DataFixtures\PersonFixtures;
use App\DataFixtures\UserFixtures;
use App\Entity\Person;
use App\Entity\User;
use App\Repository\UserRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class UserTest extends KernelTestCase
{
    use FixturesTrait;

    public function testCount()
    {
        self::bootKernel();
        $this->loadFixtures([PersonFixtures::class, UserFixtures::class]);
        $users = self::$container->get(UserRepository::class)->count([]);
        $this->assertEquals(21, $users);
    }

    public function testUserValid()
    {
        $this->validate($this->getUser(), 0);
    }

    public function testUserInvalid()
    {
        $this->validate(new User(), 5);
    }

    public function testUserPersonNull()
    {
        $this->validate(($this->getUser())->setPerson(null), 1);
    }

    public function testUserPasswordTooShort()
    {
        $this->validate(($this->getUser())->setPassword(str_repeat("a", 5)), 1);
    }

    public function testUserPasswordTooLong()
    {
        $this->validate(($this->getUser())->setPassword(str_repeat("a", 256)), 1);
    }

    public function testUserPasswordNull()
    {
        $this->validate(($this->getUser())->setPassword(null), 2);
    }

    public function testUserLoginTooShort()
    {
        $this->validate(($this->getUser())->setLogin(str_repeat("a", 3)), 1);
    }

    public function testUserLoginTooLong()
    {
        $this->validate(($this->getUser())->setLogin(str_repeat("a", 256)), 1);
    }

    public function testUserLoginNull()
    {
        $this->validate(($this->getUser())->setLogin(null), 2);
    }

    public function testUserIsAdminNull()
    {
        $this->validate(($this->getUser())->setIsAdmin(null), 0);
    }

    private function getUser()
    {
        return (new User())->setPassword("toto33990")
            ->setIsAdmin(0)
            ->setLogin("toto33")
            ->setPerson(new Person());
    }

    private function validate($user, $number)
    {
        self::bootKernel();
        $error = self::$container->get("validator")->validate($user);
        $this->assertCount($number, $error, $error);
    }
}