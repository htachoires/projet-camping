<?php

namespace App\Tests\Entity;

use App\DataFixtures\PersonFixtures;
use App\DataFixtures\UserFixtures;
use App\DataFixtures\WorkPeriodFixtures;
use App\Entity\Person;
use App\Entity\WorkPeriod;
use App\Repository\WorkPeriodRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class WorkPeriodTest extends KernelTestCase
{
    use FixturesTrait;

    public function testCount()
    {
        self::bootKernel();
        $this->loadFixtures([PersonFixtures::class, UserFixtures::class, WorkPeriodFixtures::class]);
        $workPeriods = self::$container->get(WorkPeriodRepository::class)->count([]);
        $this->assertTrue($workPeriods > 0);
    }

    public function testWorkPeriodValid(){
        $this->validate($this->getWorkPeriod(),0);
    }

    public function testWorkPeriodInvalid(){
        $this->validate(new WorkPeriod(),5);
    }

    private function getWorkPeriod()
    {
        return (new WorkPeriod())
            ->setPerson(new Person())
            ->setStartHour(8)
            ->setPauseHour(10)
            ->setResumeHour(12)
            ->setEndHour(16);
    }

    private function validate($workPeriod, $number)
    {
        self::bootKernel();
        $error = self::$container->get("validator")->validate($workPeriod);
        $this->assertCount($number, $error);
    }
}