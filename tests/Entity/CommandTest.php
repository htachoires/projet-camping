<?php

namespace App\Tests\Entity;

use App\DataFixtures\CategoryFixtures;
use App\DataFixtures\CommandFixtures;
use App\Repository\CommandRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class CommandTest extends KernelTestCase
{

    use FixturesTrait;

    public function testCount()
    {
        self::bootKernel();
        $this->loadFixtures([CommandFixtures::class]);
        $categories = self::$container->get(CommandRepository::class)->count([]);
        $this->assertEquals(500, $categories);
    }

    private function validate($command, $number)
    {
        self::bootKernel();
        $error = self::$container->get("validator")->validate($command);
        $this->assertCount($number, $error);
    }
}