<?php

namespace App\Tests\Entity;

use App\DataFixtures\IncidentFixtures;
use App\DataFixtures\SpotFixtures;
use App\Entity\Incident;
use App\Entity\Spot;
use App\Repository\IncidentRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class IncidentTest extends KernelTestCase
{
    use FixturesTrait;

    public function testCount()
    {
        self::bootKernel();
        $this->loadFixtures([SpotFixtures::class, IncidentFixtures::class]);
        $issues = self::$container->get(IncidentRepository::class)->count([]);
        $this->assertEquals(500, $issues);

    }

    public function testIncidentValid()
    {
        $this->validate($this->getIncident(), 0);
    }

    public function testIncidentInValid()
    {
        $this->validate(new Incident(), 4);
    }

    public function testIncidentSpotNull()
    {
        $this->validate(($this->getIncident())->setSpot(null), 1);
    }

    public function testIncidentCommentNull()
    {
        $this->validate(($this->getIncident())->setComment(null), 2);
    }

    public function testIncidentCommentTooLong()
    {
        $this->validate(($this->getIncident())->setComment(str_repeat("a", 256)), 1);
    }

    public function testIncidentImportantTooHigh()
    {
        $this->validate(($this->getIncident())->setImportant(3), 0);
    }

    public function testIncidentImportantTooLow()
    {
        $this->validate(($this->getIncident())->setImportant(-1), 1);
    }

    private function getIncident()
    {
        return (new Incident())
            ->setImportant(1)
            ->setComment("toto")
            ->setSpot(new Spot());
    }

    private function validate($user, $number)
    {
        self::bootKernel();
        $error = self::$container->get("validator")->validate($user);
        $this->assertCount($number, $error, $error);
    }
}