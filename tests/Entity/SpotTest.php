<?php

namespace App\Tests\Entity;

use App\DataFixtures\SpotFixtures;
use App\Entity\Spot;
use App\Repository\SpotRepository;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;

class SpotTest extends KernelTestCase
{
    use FixturesTrait;

    private $maxCapacity = 10;

    public function testCount()
    {
        self::bootKernel();
        $this->loadFixtures([SpotFixtures::class]);
        $issues = self::$container->get(SpotRepository::class)->count([]);
        $this->assertEquals(25, $issues);
    }

    public function testSpotValid()
    {
        $this->validate($this->getSpot(), 0, false);
    }

    public function testSpotAllFieldNull()
    {
        $this->validate(new Spot(), 11, false);
    }

    public function testSpotSizeInvalid()
    {
        $this->validate(($this->getSpot())->setSize(0), 1, false);
    }

    public function testSpotCapacityInvalid()
    {
        $this->validate(($this->getSpot())->setMaxCapacity(0), 1, false);
    }

    /**
     * @param Spot $spot the spot to validate
     * @param int $expected number of errors that should be throw
     * @param bool $displayErrors display errors if true else nothing
     */
    private function validate(?Spot $spot, int $expected, bool $displayErrors)
    {
        self::bootKernel();
        $validator = self::$container->get('validator');
        $errors = $validator->validate($spot);
        if ($displayErrors)
            $this->assertCount($expected, $errors, $errors);
        else
            $this->assertCount($expected, $errors);
    }

    /**
     * @return Spot Valid spot to use
     */
    private function getSpot()
    {
        $spot = (new Spot())
            ->setMaxCapacity($this->maxCapacity)
            ->setSize(15)
            ->setIsReserved(true)
            ->setLatitude(10.0)
            ->setLongitude(10.0)
            ->setIsAvailable(true)
            ->setComment("Hello World!")
            ->setHandicapFriendly(true)
            ->setHasBathroom(true)
            ->setHasElectricity(true)
            ->setHasShade(true)
            ->setHasToilet(true);

        $this->validate($spot, 0, true);

        return $spot;
    }
}