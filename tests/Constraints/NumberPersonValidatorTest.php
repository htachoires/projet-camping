<?php


namespace App\Tests\Constraints;


use App\Entity\Reservation;
use App\Entity\Spot;
use App\Validator\Constraints\NumberPerson;
use App\Validator\Constraints\NumberPersonValidator;
use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class NumberPersonValidatorTest extends ConstraintValidatorTestCase
{

    private $spot;

    private $maxCapacity = 10;

    protected function createValidator()
    {
        return new NumberPersonValidator();
    }

    protected function setUp(): void
    {
        $this->spot = new Spot();
        $this->spot->setMaxCapacity($this->maxCapacity);
        parent::setUp();
    }

    public function testIsNullValid()
    {
        $this->validate(null, 0, false);
    }

    public function testValuesNullValid()
    {
        $this->validate(new Reservation(), 0, false);
    }

    public function testSpotNullValid()
    {
        $this->validate((new Reservation())->setPersonCount($this->maxCapacity), 0, false);
    }

    public function testPersonCountNullValid()
    {
        $this->validate((new Reservation()), 0, false);
    }

    public function testPersonCountNullOfSpotValid()
    {
        $this->validate((new Reservation())->setSpot(new Spot()), 0, false);
    }

    public function testNegativeValue()
    {
        $this->validate($this->getReservation(-1), 0, false);
    }

    public function testGetterThanValue()
    {
        $this->validate($this->getReservation($this->maxCapacity + 1), 1, false);
    }

    public function testEqualValue()
    {
        $this->validate($this->getReservation($this->maxCapacity), 0, false);
    }

    public function testMinValueToMaxValue()
    {
        for ($i = 1; $i < $this->maxCapacity; $i++)
            $this->validate($this->getReservation($this->maxCapacity), 0, false);
    }

    private function getReservation($capacity)
    {
        return (new Reservation())->setPersonCount($capacity)->setSpot($this->spot);
    }

    /***
     * @param Reservation|null $reservation Reservation involved
     * @param int $expected Number of violations expected
     * @param bool $displayViolations display in console all violations messages
     */
    private function validate(?Reservation $reservation, int $expected, bool $displayViolations)
    {
        $this->validator->validate($reservation, new NumberPerson());
        $violations = $this->context->getViolations();

        if ($displayViolations) $this->displayViolations($violations);

        if ($expected === 0) {
            $this->assertNoViolation();
        } else {
            $this->assertEquals($expected, count($violations));
        }
    }

    /**
     * @param ConstraintViolationListInterface $violations
     */
    private function displayViolations(ConstraintViolationListInterface $violations)
    {
        foreach ($violations as $violation) {
            dump($violation->getMessage());
        }
    }
}