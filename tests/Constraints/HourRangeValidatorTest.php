<?php


namespace App\Tests\Constraints;


use App\Entity\ConfigVariable;
use App\Validator\Constraints\HourRange;
use App\Validator\Constraints\HourRangeValidator;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;
use InvalidArgumentException;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class HourRangeValidatorTest extends ConstraintValidatorTestCase
{
    private $MIN_HOUR;
    private $MAX_HOUR;

    private function getEntityManger()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $repo = $this->createMock(ObjectRepository::class);

        $repo->expects($this->atMost(2))->method('findBy')->willReturn(
            [(new ConfigVariable())->setValue($this->MIN_HOUR)],
            [(new ConfigVariable())->setValue($this->MAX_HOUR)]
        );
        $em->expects($this->atMost(1))->method('getRepository')->willReturn($repo);

        return $em;
    }

    protected function setUp(): void
    {
        $this->MIN_HOUR = 8;
        $this->MAX_HOUR = 12;

        parent::setUp();
    }

    protected function createValidator()
    {
        return new HourRangeValidator($this->getEntityManger());
    }

    public function testNullValue()
    {
        $this->validate(null, HourRange::$MIN_HOUR, HourRange::$MAX_HOUR, 0);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testMinIntInvalid()
    {
        $this->validate(new \DateTime("now"), HourRange::$MIN_HOUR - 1, HourRange::$MAX_HOUR, 0);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testMaxIntInvalid()
    {
        $this->validate(new \DateTime("now"), 1, HourRange::$MAX_HOUR + 1, 0);
    }

    /**
     * @expectedException Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function testMinIntNull()
    {
        $this->validate(new \DateTime("now"), null, HourRange::$MAX_HOUR, 0);
    }

    /**
     * @expectedException Symfony\Component\Validator\Exception\MissingOptionsException
     */
    public function testMinMaxIntNull()
    {
        $this->validate(new \DateTime("now"), null, null, 0);
    }

    public function testDateMaxIntValueValid()
    {
        $date = date_time_set(new \DateTime(), HourRange::$MAX_HOUR, 0, 0);
        $this->validate($date, HourRange::$MIN_HOUR, HourRange::$MAX_HOUR, 0);
    }

    public function testDateMinIntValueValid()
    {
        $date = date_time_set(new \DateTime(), HourRange::$MIN_HOUR, 0, 0);
        $this->validate($date, HourRange::$MIN_HOUR, HourRange::$MAX_HOUR, 0);
    }

    public function testDateIntValueValid()
    {
        $date = date_time_set(new \DateTime(), HourRange::$MIN_HOUR + 1, 0, 0);
        $this->validate($date, HourRange::$MIN_HOUR, HourRange::$MAX_HOUR, 0);
    }

    public function testDateMaxIntInvalid()
    {
        $date = date_time_set(new \DateTime(), HourRange::$MIN_HOUR + 1, 0, 0);
        $this->validate($date, HourRange::$MIN_HOUR, HourRange::$MAX_HOUR, 0);
    }

    public function testDateIntValueLowerThanMin()
    {
        $date = date_time_set(new \DateTime(), 10, 0, 0);
        $this->validate($date, 11, HourRange::$MAX_HOUR, 1);
    }

    public function testDateValueGreaterThanMax()
    {
        $date = date_time_set(new \DateTime(), 21, 0, 0);
        $this->validate($date, HourRange::$MIN_HOUR, 20, 1);
    }

    public function testDateValueInnerMaxAndMin()
    {
        $date = date_time_set(new \DateTime(), 10, 0, 0);
        $this->validate($date, 8, 11, 0);
    }

    /**
     * @expectedException InvalidArgumentException
     */
    public function testMaxLowerThanMinInvalid()
    {
        $this->validate(new \DateTime("now"), 20, 19, 0);
    }

    public function testMinMaxValueString()
    {
        $date = date_time_set(new \DateTime(), 10, 0, 0);
        $this->validate($date, "MIN_HOUR", "MAX_HOUR", 0);
    }

    public function testMinStringMaxIntValue()
    {
        $date = date_time_set(new \DateTime(), 10, 0, 0);
        $this->validate($date, "MIN_HOUR", $this->MAX_HOUR, 0);
    }

    private function validate(?\DateTime $date, $min, $max, int $expected)
    {
        $constraint = new HourRange(['min' => $min, 'max' => $max]);

        $this->validator->validate($date, $constraint);

        $violations = $this->context->getViolations();

        $this->assertEquals($expected, count($violations), $violations);
    }
}