<?php


namespace App\Tests\Constraints;


use App\Entity\ConfigVariable;
use App\Entity\Reservation;
use App\Entity\Spot;
use App\Validator\Constraints\StartEndDate;
use App\Validator\Constraints\StartEndDateValidator;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectRepository;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Test\ConstraintValidatorTestCase;

class StartEndValidatorTest extends ConstraintValidatorTestCase
{

    private $ARRIVAL_HOUR_START_MIN;
    private $ARRIVAL_HOUR_START_MAX;
    private $END_HOUR_START_MIN;
    private $END_HOUR_START_MAX;

    private $spot;

    private $maxCapacity = 10;

    private function getEntityManger()
    {
        $em = $this->createMock(EntityManagerInterface::class);
        $repo = $this->createMock(ObjectRepository::class);

        $repo->expects($this->atMost(5))->method('findBy')->willReturn(
            [(new ConfigVariable())->setValue($this->ARRIVAL_HOUR_START_MIN)],
            [(new ConfigVariable())->setValue($this->ARRIVAL_HOUR_START_MAX)],
            [(new ConfigVariable())->setValue($this->END_HOUR_START_MIN)],
            [(new ConfigVariable())->setValue($this->END_HOUR_START_MAX)],
            [
                $this->getReservationSpecifyingDays(5, 10, $this->ARRIVAL_HOUR_START_MIN, $this->END_HOUR_START_MAX),
                $this->getReservationSpecifyingDays(15, 20, $this->ARRIVAL_HOUR_START_MAX, $this->END_HOUR_START_MIN),
                $this->getReservationSpecifyingDays(21, 25, $this->ARRIVAL_HOUR_START_MIN, $this->END_HOUR_START_MIN),
                $this->getReservationSpecifyingDays(28, 30, $this->ARRIVAL_HOUR_START_MIN, $this->END_HOUR_START_MIN),
            ]);
        $em->expects($this->atMost(2))->method('getRepository')->willReturn($repo);

        return $em;
    }

    private function getReservationSpecifyingDays(int $startDay, int $endDay, int $startHour, int $endHour): Reservation
    {
        $startDate = date_time_set(new \DateTime($startDay . " day"), $startHour, 0, 0);
        $endDate = date_time_set(new \DateTime($endDay . " day"), $endHour, 0, 0);

        return $this->getReservation($startDate, $endDate, true);
    }

    protected function setUp(): void
    {
        $this->ARRIVAL_HOUR_START_MIN = $_ENV['ARRIVAL_HOUR_START_MIN'] ?? 8;
        $this->ARRIVAL_HOUR_START_MAX = $_ENV['ARRIVAL_HOUR_START_MAX'] ?? 11;

        $this->END_HOUR_START_MIN = $_ENV['END_HOUR_START_MIN'] ?? 8;
        $this->END_HOUR_START_MAX = $_ENV['END_HOUR_START_MAX'] ?? 11;

        $this->spot = new Spot();
        $this->spot->setMaxCapacity($this->maxCapacity);

        parent::setUp();
    }

    protected function setDefaultTimezone($defaultTimezone)
    {
        parent::setDefaultTimezone("CET");
    }


    protected function createValidator()
    {
        return new StartEndDateValidator($this->getEntityManger());
    }

    public function testNullIsValid()
    {
        $this->validate(null, 0, false);
    }

    public function testDatesAndSpotNullValid()
    {
        $this->validate(new Reservation(), 0, false);
    }

    public function testStartDateNullValid()
    {
        $reservation = new Reservation();
        $reservation->setStartDate(null);
        $reservation->setEndDate(new \DateTime());
        $reservation->setSpot(new Spot());

        $this->validate($reservation, 0, false);
    }

    public function testEndDateNullValid()
    {
        $reservation = new Reservation();
        $reservation->setStartDate(new \DateTime());
        $reservation->setEndDate(null);

        $this->validate($reservation, 0, false);
    }

    public function testSpotNullValid()
    {
        $reservation = $this->getReservation(new \DateTime(), new \DateTime(), false);

        $this->validate($reservation, 0, false);
    }

    public function testStartDateInPast()
    {
        $reservation = $this->getReservationSpecifyingDays(-1, 2, $this->ARRIVAL_HOUR_START_MIN, $this->END_HOUR_START_MIN);

        $this->validate($reservation, 1, false);
    }

    public function testStartDateNow()
    {
        $reservation = $this->getReservationSpecifyingDays(0, 2, $this->ARRIVAL_HOUR_START_MIN, $this->END_HOUR_START_MIN);

        $this->validate($reservation, 1, false);
    }

    public function testStartDateInvalidMinHour()
    {
        $reservation = $this->getReservationSpecifyingDays(1, 2, $this->ARRIVAL_HOUR_START_MIN - 1, $this->END_HOUR_START_MIN);

        $this->validate($reservation, 1, false);
    }

    public function testStartDateInvalidMaxHour()
    {
        $reservation = $this->getReservationSpecifyingDays(1, 2, $this->ARRIVAL_HOUR_START_MAX + 1, $this->END_HOUR_START_MIN);

        $this->validate($reservation, 1, false);
    }

    public function testEndDateBeforeStartDate()
    {
        $reservation = $this->getReservationSpecifyingDays(2, 1, $this->ARRIVAL_HOUR_START_MIN, $this->END_HOUR_START_MIN);

        $this->validate($reservation, 1, false);
    }

    public function testEndDateInvalidMinHour()
    {
        $reservation = $this->getReservationSpecifyingDays(1, 2, $this->ARRIVAL_HOUR_START_MIN, $this->END_HOUR_START_MIN - 1);

        $this->validate($reservation, 1, false);
    }


    public function testEndDateInvalidMaxHour()
    {
        $reservation = $this->getReservationSpecifyingDays(1, 2, $this->ARRIVAL_HOUR_START_MIN, $this->END_HOUR_START_MAX + 1);

        $this->validate($reservation, 1, false);
    }

    public function testDatesValidMinHour()
    {
        $reservation = $this->getReservationSpecifyingDays(1, 2, $this->ARRIVAL_HOUR_START_MIN, $this->END_HOUR_START_MIN);

        $this->validate($reservation, 0, false);
    }

    public function testDatesValidMaxHour()
    {
        $reservation = $this->getReservationSpecifyingDays(1, 2, $this->ARRIVAL_HOUR_START_MAX, $this->END_HOUR_START_MAX);

        $this->validate($reservation, 0, false);
    }

    public function testDatesValidMinMaxHour()
    {
        $reservation = $this->getReservationSpecifyingDays(1, 2, $this->ARRIVAL_HOUR_START_MIN, $this->END_HOUR_START_MAX);

        $this->validate($reservation, 0, false);
    }

    public function testDatesValidMaxMinHour()
    {
        $reservation = $this->getReservationSpecifyingDays(1, 2, $this->ARRIVAL_HOUR_START_MAX, $this->END_HOUR_START_MAX);

        $this->validate($reservation, 0, false);
    }

    public function testStartDateDuringAnOtherReservation()
    {
        $reservation = $this->getReservationSpecifyingDays(7, 13, $this->ARRIVAL_HOUR_START_MIN, $this->END_HOUR_START_MIN);

        $this->validate($reservation, 1, false);
    }

    public function testEndDateDuringAnOtherReservation()
    {
        $reservation = $this->getReservationSpecifyingDays(11, 16, $this->ARRIVAL_HOUR_START_MIN, $this->END_HOUR_START_MIN);

        $this->validate($reservation, 1, false);
    }

    public function testStartDateReservationSameDayAfterEndOfAnOtherReservationEnd()
    {
        $reservation = $this->getReservationSpecifyingDays(10, 14, $this->ARRIVAL_HOUR_START_MAX, $this->END_HOUR_START_MIN);

        $this->validate($reservation, 1, false);
    }

    public function testEndDateReservationSameDayAfterEndOfAnOtherReservationStart()
    {
        $reservation = $this->getReservationSpecifyingDays(11, 15, $this->ARRIVAL_HOUR_START_MIN, $this->END_HOUR_START_MIN);

        $this->validate($reservation, 1, false);
    }

    public function testReservationOnlyAfterOneDayOfAnOtherReservation()
    {
        $reservation = $this->getReservationSpecifyingDays(11, 14, $this->ARRIVAL_HOUR_START_MAX, $this->END_HOUR_START_MAX);

        $this->validate($reservation, 0, true);
    }

    public function testReservationOnlyOneDay()
    {
        $reservation = $this->getReservationSpecifyingDays(12, 13, $this->ARRIVAL_HOUR_START_MAX, $this->END_HOUR_START_MAX);

        $this->validate($reservation, 0, false);
    }

    public function testReservationOnlyOneDayBetweenOtherReservation()
    {
        $reservation = $this->getReservationSpecifyingDays(26, 27, $this->ARRIVAL_HOUR_START_MAX, $this->END_HOUR_START_MAX);

        $this->validate($reservation, 0, true);
    }

    public function testReservationAfterEveryReservation()
    {
        $reservation = $this->getReservationSpecifyingDays(35, 40, $this->ARRIVAL_HOUR_START_MAX, $this->END_HOUR_START_MAX);

        $this->validate($reservation, 0, false);
    }

    public function testReservationBeforeEveryReservation()
    {
        $reservation = $this->getReservationSpecifyingDays(1, 4, $this->ARRIVAL_HOUR_START_MAX, $this->END_HOUR_START_MAX);

        $this->validate($reservation, 0, false);
    }

    /***
     * @param \DateTime $startDate Start date of the reservation
     * @param \DateTime $endDate End date of the reservation
     * @param bool $spot if true initialize empty spot else insert null
     * @return Reservation
     */
    private function getReservation(\DateTime $startDate, \DateTime $endDate, bool $spot)
    {
        return (new Reservation())->setStartDate($startDate)->setEndDate($endDate)->setSpot($spot ? $this->spot : null);
    }

    /***
     * @param Reservation|null $reservation Reservation involved
     * @param int $expected Number of violations expected
     * @param bool $displayViolations display in console all violations messages
     */
    private function validate(?Reservation $reservation, int $expected, bool $displayViolations)
    {
        $this->validator->validate($reservation, new StartEndDate());
        $violations = $this->context->getViolations();

        if ($displayViolations) $this->displayViolations($violations);

        if ($expected === 0) {
            $this->assertNoViolation();
        } else {
            $this->assertEquals($expected, count($violations));
        }
    }

    /**
     * @param ConstraintViolationListInterface $violations
     */
    private function displayViolations(ConstraintViolationListInterface $violations)
    {
        foreach ($violations as $violation) {
            dump($violation->getMessage());
        }
    }


}