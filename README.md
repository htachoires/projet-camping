# S4P1A-E3 Camping

## Description

Ce projet a pour but de créer un logiciel de gestion pour le camping des ‘Flots Blancs’, qui est destiné à remplacer le système manuel utilisé actuellement.

Le logiciel doit faciliter la réservation et l'accueil des clients, le suivi de leur séjour et de leur satisfaction. Le logiciel devra également permettre un suivi des stocks, des factures clients et fournisseurs, et des heures de travail. Certaines statistiques seront mises à disposition pour permettre d’identifier divers problèmes et avoir une vue d’ensemble sur la gestion du Camping.

Notre rôle dans ce projet va être de concevoir l’application à partir des spécifications détaillées, d’installer la solution chez le client et de former son personnel au nouveau logiciel.


## Installation

Pour installer l'architecture de la base de donnée, lancez le script [database.sql](https://gitlab-ce.iut.u-bordeaux.fr/htachoires/projet-camping/-/raw/dev/doc/database.sql), qui ce trouve dans le dossier doc de ce git, sur une base de donnée vide. 

**ATTENTION : L'APPLICATION NE MARCHE QUE POUR LES BASE DE DONNEES MYSQL**

Installer ensuite [composer](https://getcomposer.org/download/) et [symfony](https://symfony.com/download).

Puis lancer les commandes dans le dossier camping-project
```
php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
php composer-setup.php
php -r "unlink('composer-setup.php');"
php composer.phar install
rm composer.phar
```
Pour lancer le serveur configurer la base de données dans le fichier **env.local** et **charger les données fictives**
```
php bin/console doctrine:fixtures:load [--append]
symfony serve
```
Ajouter --append pour garder vos données actuelles stockées dans la base de données

Se loger avec
 * login : *admin*
 * password : *password*

Si vous avez un problème

*  La bonne version de php est utilisée pour ce projet ?
*  Les commandes ont été éxécuté au bonne endroit ?
*  Si vous avez un problème avec PDOException, vérifier les dépendances de php

## Utilisation

Pour lancer le server entré cette commande dans ce répertoire du projet

```
symfony serve
```

Vous pouvez vous rendre sur l'application a l'adresse

```
127.0.0.1:8000/
```

## Manuel utilisateur

Le manuel utilisateur se trouve dans le dossier doc du git : [Manuel utilisateur.pdf](https://gitlab-ce.iut.u-bordeaux.fr/htachoires/projet-camping/-/blob/dev/doc/Manuel%20utilisateur.pdf)

## Tester le projet

Pour lancer les tests, ajouter la variable **DATABASE_URL** dans *.env.test.local*
```
DATABASE_URL=sqlite:///%kernel.cache_dir%/camping_test.db
```

Pour éxécuter tous les tests lancer la commande suivante
```
php bin/phpunit
```

Pour specifier une feuille de test ajouter [_--filter_] puis le nom de la classe
Exemple:
```
php bin/phpunit --filter Person
```

## Schéma de la base de donnée

![Schéma de la base de donnée](https://gitlab-ce.iut.u-bordeaux.fr/htachoires/projet-camping/-/raw/dev/doc/Schema_de_base_de_donn%C3%A9e.png)

## Bugs connus

* Suppression client et reservation ne marche pas pour une raison inconnu
* Au login, le favicon n'est pas toujours trouvé et renvoi vers 404
