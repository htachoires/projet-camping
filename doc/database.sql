CREATE TABLE `person` (
  `id` int PRIMARY KEY AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `address` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `birth_date` datetime NOT NULL,
  `phone` varchar(255) NOT NULL,
  `note` int,
  `iban` varchar(255),
  `secu_num` varchar(255)
);

CREATE TABLE `reservation` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `person_id` int NOT NULL,
  `spot_id` int NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `price` int NOT NULL,
  `person_count` int NOT NULL,
  `comment` varchar(65556)
);

CREATE TABLE `spot` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `is_reserved` boolean NOT NULL,
  `is_available` boolean NOT NULL,
  `comment` varchar(65556) NOT NULL,
  `size` int NOT NULL,
  `has_shade` boolean NOT NULL,
  `handicap_friendly` boolean NOT NULL,
  `has_electricity` boolean NOT NULL,
  `max_capacity` int NOT NULL,
  `has_bathroom` boolean NOT NULL,
  `has_toilet` boolean NOT NULL,
  `longitude` DOUBLE NOT NULL,
  `latitude` DOUBLE NOT NULL
);

CREATE TABLE `user` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `person_id` int NOT NULL,
  `login` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `is_admin` boolean NOT NULL
);

CREATE TABLE `incident` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `spot_id` int NOT NULL,
  `comment` varchar(65556) NOT NULL,
  `important` int NOT NULL,
  `date` datetime NOT NULL
);

CREATE TABLE `task` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `description` varchar(255) NOT NULL,
  `state` int NOT NULL,
  `start_date` datetime NOT NULL,
  `end_date` datetime NOT NULL,
  `incident_id` int NOT NULL
);

/*Ne pas rajouter d'id dans les tables associatives*/
CREATE TABLE `user_task` (
  `user_id` int NOT NULL,
  `task_id` int NOT NULL
);

CREATE TABLE `product` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `quantity` int NOT NULL,
  `category_id` int NOT NULL,
  `price` int NOT NULL,
  `sold_to` datetime
);

/*Ne pas rajouter d'id dans les tables associatives*/
CREATE TABLE `product_command` (
  `product_id` int NOT NULL,
  `command_id` int NOT NULL
);

CREATE TABLE `category` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL
);

CREATE TABLE `command` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `description` varchar(65556) NOT NULL,
  `supplier` varchar(255) NOT NULL,
  `bill` blob,
  `command_date` datetime NOT NULL,
  `delivery_date` datetime
);

CREATE TABLE `work_period` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `person_id` int NOT NULL,
  `date` datetime NOT NULL,
  `start_hour` int NOT NULL,
  `pause_hour` int NOT NULL,
  `resume_hour` int NOT NULL,
  `end_hour` int NOT NULL
);

CREATE TABLE `history` (
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL,
  `date` datetime NOT NULL,
  `description` varchar(255) NOT NULL
);

CREATE TABLE `config_variable`(
  `id` int PRIMARY KEY NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `value` int NOT NULL,
  `updated_at` datetime NOT NULL
);

/*Relation entre user et task*/
ALTER TABLE `user_task`
  ADD PRIMARY KEY (`user_id`,`task_id`),
  ADD KEY `pk_user_task_user_id` (`user_id`),
  ADD KEY `pk_user_task_task_id` (`task_id`);

/*Relation entre product et command*/
ALTER TABLE `product_command`
  ADD PRIMARY KEY (`product_id`,`command_id`),
  ADD KEY `pk_product_command_product_id` (`product_id`),
  ADD KEY `pk_product_command_command_id` (`command_id`);


ALTER TABLE task ADD FOREIGN KEY (incident_id) REFERENCES incident(id);
ALTER TABLE user ADD FOREIGN KEY (person_id) REFERENCES person(id);
ALTER TABLE product ADD FOREIGN KEY (category_id) REFERENCES category(id);
ALTER TABLE incident ADD FOREIGN KEY (spot_id) REFERENCES spot(id);
ALTER TABLE user_task ADD FOREIGN KEY (user_id) REFERENCES user(id);
ALTER TABLE user_task ADD FOREIGN KEY (task_id) REFERENCES task(id);
ALTER TABLE reservation ADD FOREIGN KEY (person_id) REFERENCES person(id);
ALTER TABLE reservation ADD FOREIGN KEY (spot_id) REFERENCES spot(id);
ALTER TABLE product_command ADD FOREIGN KEY (product_id) REFERENCES product(id);
ALTER TABLE product_command ADD FOREIGN KEY (command_id) REFERENCES command(id);
ALTER TABLE history ADD FOREIGN KEY (user_id) REFERENCES user(id);
ALTER TABLE work_period ADD FOREIGN KEY (person_id) REFERENCES person(id);
